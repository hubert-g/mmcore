/*
MIT License

Copyright (c) 2015-2018 Hubert Grzegorczyk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#ifndef KALMAN_MAZE_H_
#define KALMAN_MAZE_H_


#include "arm_math.h"
#include "speed_pos_profiler.h"
#include "position_real.h"


//Kinematyka, kalman
//metry

typedef struct{
	uint8_t global_direction; //0 err, 1,2,3,4 N,E,W,S, gdzie N to +y i E +x
	float w_cord; //w zaleznosci od global dir jest to y lub x //wall cord in global
	float another_cord; //druga wspolrzedna x/y
}theta_meas_data;

typedef struct {
	float meas;
	float Ht_row[3];
	theta_meas_data theta_meas;
}predicted_sensor_meas;


typedef struct {
	float meas;
	position_real predicted_pos_at_meas_time;
	uint8_t new_meas;
}sensor_meas_snapshot;

//parameters structures

typedef struct{
	float var_x;
	float var_y;
	float var_theta;
	float var_vl;
	float var_theta_meas;
	uint8_t theta_meas_gap;

	float vl_predict_meas_allowed_delta;

	float distance_from_wall_edge;
	float distance_from_wall_edge_further_walls;
	float theta_meas_min_dist_between_2_meas;
	uint8_t search_box_size;

	position_real sensors_position_in_robot_frame[3];
}kalman_parameters;

typedef struct{
	kalman_parameters params;

	position_real mean;
	position_real mean_predicted;
	arm_matrix_instance_f32 covariance;
	sensor_meas_snapshot sensors_meas[3];
	arm_matrix_instance_f32 Gt;
	arm_matrix_instance_f32 Rt;
	arm_matrix_instance_f32 Gt_tran;
	arm_matrix_instance_f32 ans;
	arm_matrix_instance_f32 covariance_predict;
	arm_matrix_instance_f32 Ht;
	arm_matrix_instance_f32 Ht_tran;
	arm_matrix_instance_f32 Qt;
	arm_matrix_instance_f32 ans2;
	arm_matrix_instance_f32 Kt;
	arm_matrix_instance_f32 delta_zt_zt_predict;
	arm_matrix_instance_f32 ansVector;
	arm_matrix_instance_f32 identity_3;

	float Gt_data[9];
	float Gt_tran_data[9];
	float Rt_data[9];
	float ans_data[25];
	float covariance_data[9];
	float covariance_predict_data[9];
	float Ht_data[15];
	float Ht_tran_data[15];
	float Qt_data[25];
	float ans2_data[25];
	float Kt_data[25];
	float delta_zt_data[5];
	float ans_vector_data[3];
	float identity_3_data[9];


	theta_meas_data left_vl_hist[10];
	theta_meas_data right_vl_hist[10];
	uint8_t index_left;
	uint8_t index_right;

	uint8_t delta_pos_meas_on;

}kalman_maze_data;



float angular_distance(float from,float to);
void kalman_set_position_to_known(kalman_maze_data *data, position_real known_pos);
void kalman_move_mean_predicted(kalman_maze_data *data, float speed_tran, float speed_rot, float deltaT);
void kalman_init_data(kalman_maze_data *data,position_real start_pos);
void kalman_step(kalman_maze_data *data,float speed_tran,float deltaT,uint8_t (*map)[16]);
void kalman_delta_pos_meas_on_off(kalman_maze_data *data,int8_t state);


#endif /* KALMAN_MAZE_H_ */
