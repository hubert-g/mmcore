/*
MIT License

Copyright (c) 2015-2018 Hubert Grzegorczyk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#ifndef MICROMOUSE_H_
#define MICROMOUSE_H_

#include "position_real.h"
#include "maze.h"
#include "kalman_maze.h"
#include "speed_pos_profiler.h"



typedef struct {
	maze_data *maze_dat;
	speed_pos_data *speed_pos_profiler_data;
	kalman_maze_data *kalman_data;
	uint8_t finish_x;
	uint8_t finish_y;
	position_real start_position;

	//parameters
	speed_pos_euler_turn_data_index transition_curve_theta;
	float distance_from_square_cross_for_scanning;
	float distance_from_back_to_robot_rotate_center;
	maze_global_dirs start_global_dir;

	float scan_acc;
	float scan_deacc;

	float speedrun_acc;
	float speedrun_deacc;

	//flags, when 1 u have to follow path given by speeds_pos_profiler
	//path control is divided on straight parts ( in parallel to walls), diagonals,
	//turns (when robot is doing turn or some maneuvers), rotate (in place)
	uint8_t follow_path_straight:1;
	uint8_t follow_path_diagonal:1;
	uint8_t follow_path_turn:1;
	uint8_t follow_path_rotate:1;

	uint8_t stopped:1;//jesli stoje na srodku kratki to 1. potrzebne bo normalnie algorytm mapowania zak�ada ze jestem w ciag�ym ruchu
		///jesli jade to decyduje co robic na przecieciu kratek,albo kawa�ek za, zalezy od zmiennej distance_from_square_cross_for_scanning
	//okresla ona jak daleko od przeciecia kratek robie skan
}micromouse_data;

typedef enum{
	MICROMOUSE_DIAGONAL_SPEEDRUN,
	MICROMOUSE_NORMAL_SPEEDRUN
}micromouse_speedrun_type;


uint8_t micromouse_check_if_sufficiently_mapped(micromouse_data *mm_data);
uint8_t micromouse_speedrun(micromouse_data *mm_data,
		micromouse_speedrun_type speedrun_type, float speed_straight,
		float speed_turn);
uint8_t micromouse_scan_from_start_to_finish_and_back(micromouse_data *mm_data, float speed_scanning) ;

char *draw_small_map_to_string(uint8_t (*map)[16], unsigned int *strlen);
char *print_route_to_string(struct maze_route_list* route,unsigned int *strlen);

#endif /* MICROMOUSE_H_ */
