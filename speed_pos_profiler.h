/*
MIT License

Copyright (c) 2015-2018 Hubert Grzegorczyk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#ifndef SPEED_POS_PROFILER_H_
#define SPEED_POS_PROFILER_H_


#include "stdint.h"
#include "position_real.h"

//dane klotoidy jednostkowej dla 45 stopni
#define EULER_SPIRAL_1_45DEG_L 1.25331413
#define EULER_SPIRAL_1_45DEG_X 1.17960471
#define EULER_SPIRAL_1_45DEG_Y 0.31536875

typedef enum{
	EULER_TURN_5DEG=0,
	EULER_TURN_10DEG,
	EULER_TURN_15DEG,
	EULER_TURN_20DEG,
	EULER_TURN_30DEG,
	EULER_TURN_INDEX_MAX
}speed_pos_euler_turn_data_index;


#define SMALL_ERROR 0.0001

typedef enum{
	SPEED_POS_PROFILER_STATE_RUNNING,
	SPEED_POS_PROFILER_STATE_BRAKING,
	SPEED_POS_PROFILER_STATE_FINISHED
}speed_pos_profiler_state;


typedef struct{
	float v_trans;
	float v_rot;
}speeds;


typedef struct {
	position_real ideal_position_at_move_start;
	position_real ideal_current_pos;
	position_real ideal_position_at_move_end;


	float acc;
	float curr_speed;
	float dest_speed;
	float final_speed;
	float braking_acc;
	speed_pos_profiler_state state_trans;
	int8_t move_direction; //jeden do przodu, dodatnio w osi x robota

	float distance_to_move;

	float acc_rot;
	float curr_speed_rot;
	float dest_speed_rot;
	float final_speed_rot;
	float braking_acc_rot;
	int8_t move_direction_rot;// jeden to CCW, jak w uk�adzie wsp, -1 CW
	speed_pos_profiler_state state_rot;
	float distance_to_move_rot;

	float euler_turn_a;
	float euler_turn_half_L;
	speed_pos_profiler_state state_euler_turn;

	float turn_transition_L;
	float turn_transition_a;
	float turn_radius;
	float turn_arc_L;
	speed_pos_profiler_state state_turn;

} speed_pos_data;



float parse_angle_to_minus_pi_to_pi(float angle);

uint8_t inline speed_pos_is_moving(volatile speed_pos_data *data){
	return (data->state_rot!=SPEED_POS_PROFILER_STATE_FINISHED || data->state_trans!=SPEED_POS_PROFILER_STATE_FINISHED);
}


void speed_pos_profiler_init_data(speed_pos_data *data,speeds *curr_speeds);

void speed_pos_set_ideal_position(speed_pos_data *data,position_real current_position);

void speed_pos_profiler_step(speed_pos_data *data, float deltaT,
		speeds *current_speed_given, position_real *current_position_given);

/*
 *
 *
 *                         /
 *                        /
 *                       /
 *                      / b
 *                     /
 *                    /
 * __________________/alpha)
 *          b
 *  Theta is the angle of transition curve on bothe sides of turn.
 *  Turn is symmetric. It is composed from two euler spirals and and arc part.
 *  Everything in radians, m/s, m/s^2. Alpha must be 0-135 degrees.
 *
 */
uint8_t speed_pos_turn(speed_pos_data *data, float b,
		speed_pos_euler_turn_data_index theta, float alpha, float speed,
		float lin_acc, float lin_deacc, float end_speed);



void speed_pos_euler_spiral_turn_90(speed_pos_data *data,float dx_dy, float speed, float end_speed,
		float lin_acc, float lin_deacc, int8_t direction);
/*
 * m , m/s , m/s^2
 * Final speed need to be lower than speed!
 *		____ speed
 * 	   /	\
 * ___/		 \___ final_speed
 *
 */
void speed_pos_forward(speed_pos_data *data,float speed,float acc, float deacc,float distance,float final_speed);

/*
 * rad and rad/s , rad/s^2
 * Final speed need to be lower than speed!
 * 		____ speed
 * 	   /	\
 * ___/		 \___ final_speed
 *
 */
void speed_pos_rotate(speed_pos_data *data,float speed,float acc, float deacc,float distance,float final_speed);





#endif /* SPEED_POS_PROFILER_H_ */
