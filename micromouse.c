/*
MIT License

Copyright (c) 2015-2018 Hubert Grzegorczyk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "micromouse.h"
#include "position_real.h"
#include "maze.h"
#include "kalman_maze.h"
#include "speed_pos_profiler.h"
#include "mem_management.h"
#include "main.h"

//IMPORTANT, this function assume robot is stopped at the center of the square.
uint8_t micromouse_scan_to_target(micromouse_data *mm_data, float speed_scanning,uint8_t target_x, uint8_t target_y){

	maze_local_dirs next_move;
	uint8_t ret;

	while (1) {
		//////////////////////////ALGORYTM Z KALMANEM

		ret = maze_searching_get_next_move_to_target(mm_data->maze_dat,
				check_wall_left(), check_wall_right(), check_wall_front(),
				target_x, target_y, &next_move);

		if (ret == 0) {
			switch (next_move) {
			case MAZE_LOCAL_DIR_AHEAD:
				while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
					;
				if (mm_data->stopped) {

					speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning,
							mm_data->scan_acc, mm_data->scan_deacc,
							0.09 - 0.005 + mm_data->distance_from_square_cross_for_scanning,
							speed_scanning);
					mm_data->follow_path_straight=1;
					kalman_delta_pos_meas_on_off(mm_data->kalman_data, 1);
					while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
						;
					speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning,
							mm_data->scan_acc, mm_data->scan_deacc, 0.005, speed_scanning);
					mm_data->stopped = 0;
				} else {

					speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning,
							mm_data->scan_acc, mm_data->scan_deacc, 0.18 - 0.005, speed_scanning);
					mm_data->follow_path_straight=1;
					kalman_delta_pos_meas_on_off(mm_data->kalman_data, 1);
					while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
						;
					speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning,
							mm_data->scan_acc, mm_data->scan_deacc, 0.005, speed_scanning);
				}
				maze_move_position_forward(&(mm_data->maze_dat->cur_pos));
				break;
			case MAZE_LOCAL_DIR_RIGHT:
				while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
					;

				maze_turn_course_CW(&(mm_data->maze_dat->cur_pos));

				if (mm_data->stopped) {

					mm_data->follow_path_straight=0;
					kalman_delta_pos_meas_on_off(mm_data->kalman_data, 0);
					speed_pos_rotate(mm_data->speed_pos_profiler_data, M_PI * 4,
					M_PI * 4, M_PI * 4, -M_PI_2, 0);
					mm_data->follow_path_rotate=1;
					while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
						;
					mm_data->follow_path_rotate=0;

					speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning,
							mm_data->scan_acc, mm_data->scan_deacc,
							0.09 - 0.005 + mm_data->distance_from_square_cross_for_scanning,
							speed_scanning);
					mm_data->follow_path_straight=1;
					kalman_delta_pos_meas_on_off(mm_data->kalman_data, 1);
					while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
						;

					speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning,
							mm_data->scan_acc, mm_data->scan_deacc, 0.005, speed_scanning);
					mm_data->stopped = 0;
				} else {

					mm_data->follow_path_straight=0;
					mm_data->follow_path_turn=1;
					kalman_delta_pos_meas_on_off(mm_data->kalman_data, 0);

					speed_pos_turn(mm_data->speed_pos_profiler_data,
							0.09 - mm_data->distance_from_square_cross_for_scanning,
							mm_data->transition_curve_theta, -M_PI_2, speed_scanning, mm_data->scan_acc,
							mm_data->scan_deacc, speed_scanning);

					while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
						;
					mm_data->follow_path_turn=0;
					mm_data->follow_path_straight=1;
					kalman_delta_pos_meas_on_off(mm_data->kalman_data, 1);
					speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning,
							mm_data->scan_acc, mm_data->scan_deacc,
							2 * mm_data->distance_from_square_cross_for_scanning - 0.005,
							speed_scanning);
					while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
						;

					speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning,
							mm_data->scan_acc, mm_data->scan_deacc, 0.005, speed_scanning);

				}
				maze_move_position_forward(&(mm_data->maze_dat->cur_pos));
				break;
			case MAZE_LOCAL_DIR_LEFT:
				while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
					;

				maze_turn_course_CCW(&(mm_data->maze_dat->cur_pos));

				if (mm_data->stopped) {

					mm_data->follow_path_straight=0;
					kalman_delta_pos_meas_on_off(mm_data->kalman_data, 0);
					speed_pos_rotate(mm_data->speed_pos_profiler_data, M_PI * 4,
					M_PI * 4, M_PI * 4, M_PI_2, 0);
					mm_data->follow_path_rotate=1;
					while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
						;
					mm_data->follow_path_rotate=0;

					speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning,
							mm_data->scan_acc, mm_data->scan_deacc,
							0.09 - 0.005 + mm_data->distance_from_square_cross_for_scanning,
							speed_scanning);
					mm_data->follow_path_straight=1;
					kalman_delta_pos_meas_on_off(mm_data->kalman_data, 1);
					while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
						;

					speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning,
							mm_data->scan_acc, mm_data->scan_deacc, 0.005, speed_scanning);
					mm_data->stopped = 0;
				} else {
					mm_data->follow_path_straight=0;
					mm_data->follow_path_turn=1;
					kalman_delta_pos_meas_on_off(mm_data->kalman_data, 0);

					speed_pos_turn(mm_data->speed_pos_profiler_data,
							0.09 - mm_data->distance_from_square_cross_for_scanning,
							mm_data->transition_curve_theta, M_PI_2, speed_scanning, mm_data->scan_acc,
							mm_data->scan_deacc, speed_scanning);
					while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
						;
					mm_data->follow_path_turn=0;
					mm_data->follow_path_straight=1;
					kalman_delta_pos_meas_on_off(mm_data->kalman_data, 1);

					speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning,
							mm_data->scan_acc, mm_data->scan_deacc,
							2 * mm_data->distance_from_square_cross_for_scanning - 0.005,
							speed_scanning);
					while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
						;

					speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning,
							mm_data->scan_acc, mm_data->scan_deacc, 0.005, speed_scanning);

				}
				maze_move_position_forward(&(mm_data->maze_dat->cur_pos));
				break;
			case MAZE_LOCAL_DIR_BEHIND:
				while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
					;

				if (mm_data->stopped == 0) {
					speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning,
							mm_data->scan_acc, mm_data->scan_deacc, 0.09 - mm_data->distance_from_square_cross_for_scanning,
							0);
					kalman_delta_pos_meas_on_off(mm_data->kalman_data, 1);
					mm_data->follow_path_straight=1;
					while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
						;
					kalman_delta_pos_meas_on_off(mm_data->kalman_data, 0);
					mm_data->follow_path_straight=0;

				} else {
					mm_data->stopped = 0;
				}

				kalman_delta_pos_meas_on_off(mm_data->kalman_data, 0);
				mm_data->follow_path_straight=0;
				speed_pos_rotate(mm_data->speed_pos_profiler_data, M_PI * 4,
				M_PI * 4, M_PI * 4, M_PI, 0);
				mm_data->follow_path_rotate=1;
				while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
					;
				mm_data->follow_path_rotate=0;

				speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning, mm_data->scan_acc,
						mm_data->scan_deacc, 0.09 - 0.005 + mm_data->distance_from_square_cross_for_scanning,
						speed_scanning);
				kalman_delta_pos_meas_on_off(mm_data->kalman_data, 1);
				mm_data->follow_path_straight=1;
				while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
					;

				speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning, mm_data->scan_acc,
						mm_data->scan_deacc, 0.005, speed_scanning);
				maze_turn_course_CCW(&(mm_data->maze_dat->cur_pos));
				maze_turn_course_CCW(&(mm_data->maze_dat->cur_pos));
				maze_move_position_forward(&(mm_data->maze_dat->cur_pos));
				break;

			}
		} else if (ret == 1) { //dotarlem do celu
			if (mm_data->stopped == 0) {
				mm_data->stopped = 1;
				speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning, mm_data->scan_acc,
						mm_data->scan_deacc, 0.09 - mm_data->distance_from_square_cross_for_scanning, 0);
				while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
					;

				mm_data->follow_path_straight=0;
				mm_data->follow_path_turn=0;
				mm_data->follow_path_rotate=0;
				kalman_delta_pos_meas_on_off(mm_data->kalman_data, 0);

			}
			return 0;
		} else if (ret == 2) {

			if (mm_data->stopped == 0) {
				mm_data->stopped = 1;
				speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning, mm_data->scan_acc,
						mm_data->scan_deacc, 0.09 - mm_data->distance_from_square_cross_for_scanning, 0);
				while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
					;

				mm_data->follow_path_straight=0;
				mm_data->follow_path_turn=0;
				mm_data->follow_path_rotate=0;
				kalman_delta_pos_meas_on_off(mm_data->kalman_data, 0);

				log_string("Route to target not found", 0, 0);
				unsigned int len;
				char *str = draw_small_map_to_string(mm_data->maze_dat->map, &len);
				log_string(str, 1, len);
			}
			return 1;
		}

	}
return 2;
}

/*
 * Robot goes to target and back while scanning map.
 *
 * Robot has to be at the start_position, touching wall with its back.
 * start_global_dir, and start_position have to be properly filled.
 *
 */
uint8_t micromouse_scan_from_start_to_finish_and_back(micromouse_data *mm_data, float speed_scanning) {

	uint8_t ret;
	unsigned int len;
	char *text;

	kalman_set_position_to_known(mm_data->kalman_data, mm_data->start_position);
	speed_pos_set_ideal_position(mm_data->speed_pos_profiler_data, mm_data->start_position);

	mm_data->stopped = 1;

	log_string("Scanning from start to finish and back", 0, 0);

	//zaczynam dotykaj�c ty�em scianki i na srodku! sam wymierzam, wa�ne!!
	speed_pos_forward(mm_data->speed_pos_profiler_data, speed_scanning, 1.0, 0.8,
			0.09 - 0.006 - mm_data->distance_from_back_to_robot_rotate_center, 0.0);
	while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
		;


	ret = micromouse_scan_to_target(mm_data,speed_scanning,mm_data->finish_x,mm_data->finish_y);

	if(ret){
		//no path to finish, go back on start
		micromouse_scan_to_target(mm_data,speed_scanning,0,0);
		return ret;
	}

	log_string("Arrived at finish", 0, 0);
	text = draw_small_map_to_string(mm_data->maze_dat->map, &len);
	log_string(text, 1, len);

	ret = micromouse_scan_to_target(mm_data,speed_scanning,0,0);

	if(ret==0){
		log_string("Arrived at start", 0, 0);
		text = draw_small_map_to_string(mm_data->maze_dat->map, &len);
		log_string(text, 1, len);

		//ustawic sie tylem i do sciany dotkn�c
		while (mm_data->maze_dat->cur_pos.course != mm_data->start_global_dir){
			speed_pos_rotate(mm_data->speed_pos_profiler_data, M_PI * 4,
			M_PI * 4, M_PI * 4, -M_PI_2, 0);
			mm_data->follow_path_rotate=1;
			while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
				;
			mm_data->follow_path_rotate=0;
			maze_turn_course_CW(&(mm_data->maze_dat->cur_pos));
		}
		speed_pos_forward(mm_data->speed_pos_profiler_data, 0.1, 0.2, 0.2,
				-(0.090 - 0.006 - mm_data->distance_from_back_to_robot_rotate_center), 0);
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;
		///////
	}

	return ret;
}


//0 -path ok
//1 -there is no path to finish
//2 -path is going through not visited squares
uint8_t micromouse_check_if_sufficiently_mapped(micromouse_data *mm_data){
	//with NULL will only check, route list will not be allocated
	maze_do_floodfill(mm_data->maze_dat->floodfill, mm_data->maze_dat->map, mm_data->finish_x, mm_data->finish_y);
	return maze_floodfill_to_route_for_speedrun(mm_data->maze_dat->floodfill, mm_data->maze_dat->map,
			NULL, mm_data->finish_x, mm_data->finish_y, mm_data->maze_dat->cur_pos);
}



/*
 *
 * return:
 * 0 ok
 * 1 no path to finish
 * 2 route going through not visited squares
 * 5 no route empty
 */
uint8_t micromouse_speedrun(micromouse_data *mm_data,
		micromouse_speedrun_type speedrun_type, float speed_straight,
		float speed_turn) {

	struct maze_route_list *route = NULL;
	uint8_t ret = 0;

	unsigned int len;
	char *text;

	kalman_set_position_to_known(mm_data->kalman_data, mm_data->start_position);
	speed_pos_set_ideal_position(mm_data->speed_pos_profiler_data, mm_data->start_position);

	switch(speedrun_type){
	case MICROMOUSE_NORMAL_SPEEDRUN:
		ret = maze_generate_route_for_speedrun(&route, mm_data->maze_dat,
				mm_data->finish_x,mm_data->finish_y);
		if(ret){
			maze_route_list_delete(&route);
			return ret;
		}


		log_string("Speedrun without diagonals.", 0, 0);
		text = print_route_to_string(route, &len);
		log_string(text, 1, len);

		break;
	case MICROMOUSE_DIAGONAL_SPEEDRUN:
		ret = maze_generate_route_for_speedrun_with_diagonals(&route, mm_data->maze_dat,
				mm_data->finish_x,mm_data->finish_y,2);
		if(ret){
			maze_route_list_delete(&route);
			return ret;
		}

		log_string("Speedrun with diagonals.", 0, 0);
		text = print_route_to_string(route, &len);
		log_string(text, 1, len);

		break;
	}


	//pierwszy ruch to tylko rozpedzienie
	struct maze_route_list* move = maze_route_list_get_from_top(&route);

	if(move==NULL){
		return 5;
	}

	mm_data->follow_path_straight=0;
	mm_data->follow_path_diagonal=0;
	mm_data->follow_path_turn=0;
	mm_data->follow_path_rotate=0;
	kalman_delta_pos_meas_on_off(mm_data->kalman_data,0);

	log_string("Speedrun started",0,0);

	if (move->direction == MAZE_LOCAL_DIR_RIGHT) {
		speed_pos_forward(mm_data->speed_pos_profiler_data, 0.2, 0.2, 0.2,
				0.09 - 0.006 - mm_data->distance_from_back_to_robot_rotate_center, 0);
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;
		speed_pos_rotate(mm_data->speed_pos_profiler_data, M_PI * 4, M_PI * 4, M_PI * 4,
				-M_PI_2, 0);
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;

		speed_pos_forward(mm_data->speed_pos_profiler_data, speed_turn, mm_data->speedrun_acc, mm_data->speedrun_deacc,
				0.09, speed_turn);
		kalman_delta_pos_meas_on_off(mm_data->kalman_data,1);
		mm_data->follow_path_straight=1;

		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;
		mm_data->follow_path_straight=0;
		kalman_delta_pos_meas_on_off(mm_data->kalman_data, 0);

	} else if (move->direction == MAZE_LOCAL_DIR_AHEAD) {
		speed_pos_forward(mm_data->speed_pos_profiler_data, speed_straight, mm_data->speedrun_acc,
		mm_data->speedrun_deacc, move->distance - 0.006 - mm_data->distance_from_back_to_robot_rotate_center,  speed_turn);
		kalman_delta_pos_meas_on_off(mm_data->kalman_data,1);
		mm_data->follow_path_straight=1;
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;
		kalman_delta_pos_meas_on_off(mm_data->kalman_data, 0);
		mm_data->follow_path_straight=0;

	} else if (move->direction == MAZE_LOCAL_DIR_LEFT) {
		speed_pos_forward(mm_data->speed_pos_profiler_data, 0.2, 0.2, 0.2,
				0.09 - 0.006 - mm_data->distance_from_back_to_robot_rotate_center, 0);
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;
		speed_pos_rotate(mm_data->speed_pos_profiler_data, M_PI * 4, M_PI * 4, M_PI * 4,
				M_PI_2, 0);
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;

		kalman_delta_pos_meas_on_off(mm_data->kalman_data,1);
		mm_data->follow_path_straight=1;
		speed_pos_forward(mm_data->speed_pos_profiler_data, speed_turn, mm_data->speedrun_acc, mm_data->speedrun_deacc,
				0.09,  speed_turn);
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;
		kalman_delta_pos_meas_on_off(mm_data->kalman_data, 0);
		mm_data->follow_path_straight=0;
	}else if(move->direction == MAZE_LOCAL_DIR_AHEAD_LEFT){
		//zakladam ze ten obrot jest w miejscu, musi byc
		move->next->distance-=0.09*M_SQRT2;
		//ma srodek jade
		speed_pos_forward(mm_data->speed_pos_profiler_data, 0.2, 0.2, 0.2,
				0.065 - 0.006 - mm_data->distance_from_back_to_robot_rotate_center, 0);
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;
		speed_pos_rotate(mm_data->speed_pos_profiler_data, M_PI * 4, M_PI * 4, M_PI * 4,
				M_PI_2, 0);
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;
		speed_pos_forward(mm_data->speed_pos_profiler_data, 0.2, 0.2, 0.2,
				0.09-(1+M_SQRT2)*0.025, 0.2);
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;
		speed_pos_turn(mm_data->speed_pos_profiler_data,0.025*M_SQRT2,mm_data->transition_curve_theta,-M_PI_4,speed_turn, 1,1.0,speed_turn);
		mm_data->follow_path_turn=1;
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;
		mm_data->follow_path_turn=0;

	}else if(move->direction == MAZE_LOCAL_DIR_AHEAD_RIGHT){
		//zakladam ze ten obrot jest w miejscu, musi byc
		move->next->distance-=0.09*M_SQRT2;
		//ma srodek jade
		speed_pos_forward(mm_data->speed_pos_profiler_data, 0.2, 0.2, 0.2,
				0.065 - 0.006 - mm_data->distance_from_back_to_robot_rotate_center, 0);
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;
		speed_pos_rotate(mm_data->speed_pos_profiler_data, M_PI * 4, M_PI * 4, M_PI * 4,
				-M_PI_2, 0);
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;
		speed_pos_forward(mm_data->speed_pos_profiler_data, 0.2, 0.2, 0.2,
				0.09-(1+M_SQRT2)*0.025, 0.2);
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;
		speed_pos_turn(mm_data->speed_pos_profiler_data,0.025*M_SQRT2,mm_data->transition_curve_theta,M_PI_4,speed_turn,1,1.0,speed_turn);
		mm_data->follow_path_turn=1;
		while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
			;
		mm_data->follow_path_turn=0;
	}

	free_adapt(move);
	move = maze_route_list_get_from_top(&route);

	while (move != NULL) {
		switch (move->direction) {
		case MAZE_LOCAL_DIR_AHEAD:
			kalman_delta_pos_meas_on_off(mm_data->kalman_data,1);

			if(maze_move_is_diagonal(move)){
				mm_data->follow_path_diagonal=1;
			}else{
				mm_data->follow_path_straight=1;
			}

			if (move->next == NULL) {
				speed_pos_forward(mm_data->speed_pos_profiler_data, speed_straight, mm_data->speedrun_acc,
				mm_data->speedrun_deacc,move->distance , 0);
				while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
					;

				//go back small distance imidietely!
				speed_pos_forward(mm_data->speed_pos_profiler_data, speed_straight, mm_data->speedrun_acc,
				mm_data->speedrun_deacc,-0.03 , 0);
				while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
					;

				if(maze_move_is_diagonal(move)){
					mm_data->follow_path_diagonal=0;
				}else{
					mm_data->follow_path_straight=0;
				}
			} else {
				speed_pos_forward(mm_data->speed_pos_profiler_data, speed_straight, mm_data->speedrun_acc,
				mm_data->speedrun_deacc, move->distance,speed_turn);
				while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
					;
				if(maze_move_is_diagonal(move)){
					mm_data->follow_path_diagonal=0;
				}else{
					mm_data->follow_path_straight=0;
				}
			}
			kalman_delta_pos_meas_on_off(mm_data->kalman_data, 0);

			break;

		case MAZE_LOCAL_DIR_RIGHT:
			mm_data->follow_path_turn=1;
			kalman_delta_pos_meas_on_off(mm_data->kalman_data,0);
			speed_pos_turn(mm_data->speed_pos_profiler_data,move->distance,mm_data->transition_curve_theta,-M_PI_2,speed_turn,1,1,speed_turn);
			while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
				;
			mm_data->follow_path_turn=0;
			break;

		case MAZE_LOCAL_DIR_LEFT:
			mm_data->follow_path_turn=1;
			kalman_delta_pos_meas_on_off(mm_data->kalman_data,0);
			speed_pos_turn(mm_data->speed_pos_profiler_data,move->distance,mm_data->transition_curve_theta,M_PI_2,speed_turn,1,1,speed_turn);
			while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
				;
			mm_data->follow_path_turn=0;
			break;
		case MAZE_LOCAL_DIR_AHEAD_LEFT:
			mm_data->follow_path_turn=1;
			kalman_delta_pos_meas_on_off(mm_data->kalman_data,0);
			speed_pos_turn(mm_data->speed_pos_profiler_data, move->distance,
					mm_data->transition_curve_theta, M_PI_4, speed_turn, 1, 1, speed_turn);
			while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
				;
			mm_data->follow_path_turn=0;
			break;
		case MAZE_LOCAL_DIR_AHEAD_RIGHT:
			mm_data->follow_path_turn=1;
			kalman_delta_pos_meas_on_off(mm_data->kalman_data,0);
			speed_pos_turn(mm_data->speed_pos_profiler_data, move->distance,
					mm_data->transition_curve_theta, -M_PI_4, speed_turn, 1, 1, speed_turn);
			while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
				;
			mm_data->follow_path_turn=0;
			break;
		case MAZE_LOCAL_DIR_BEHIND_LEFT:
			mm_data->follow_path_turn=1;
			kalman_delta_pos_meas_on_off(mm_data->kalman_data,0);
			speed_pos_turn(mm_data->speed_pos_profiler_data, move->distance,
					mm_data->transition_curve_theta, M_PI_4 * 3, speed_turn, 1, 1,
					speed_turn);
			while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
				;
			mm_data->follow_path_turn=0;
			break;
		case MAZE_LOCAL_DIR_BEHIND_RIGHT:
			mm_data->follow_path_turn=1;
			kalman_delta_pos_meas_on_off(mm_data->kalman_data,0);
			speed_pos_turn(mm_data->speed_pos_profiler_data, move->distance,
					mm_data->transition_curve_theta, -M_PI_4 * 3, speed_turn, 1, 1,
					speed_turn);
			while (speed_pos_is_moving(mm_data->speed_pos_profiler_data))
				;
			mm_data->follow_path_turn=0;
			break;

		}

		free_adapt(move);
		move = maze_route_list_get_from_top(&route);
	}

	log_string("Speedrun ended",0,0);

	maze_route_list_delete(&route);

	kalman_delta_pos_meas_on_off(mm_data->kalman_data, 0);
	mm_data->follow_path_straight=0;
	mm_data->follow_path_diagonal=0;
	mm_data->follow_path_turn=0;
	mm_data->follow_path_rotate=0;
	return 0; //bez bledu
}


/*
 * returns allocated string with route and strlen vie ref
 *
 */
char *print_route_to_string(struct maze_route_list *route,unsigned int *strlen){
	uint16_t how_many_moves=0;
	struct maze_route_list* temp=route;
	char *text;
	*strlen=0;

	while(temp!=NULL){
		temp=temp->next;
		++how_many_moves;
	}

	//na poczatku jest \r\n
	//w srodku how_many_moves linii po x znakow
	//plus znak konca tekstu
	how_many_moves=22*how_many_moves+2+1;
	text=malloc_adapt(how_many_moves);

	text[(*strlen)++]='\r';
	text[(*strlen)++]='\n';

	while(route!=NULL){
		//3 symbole
		switch(route->direction){
		case MAZE_LOCAL_DIR_AHEAD:
			text[(*strlen)++]='A';
			text[(*strlen)++]=' ';
			break;
		case MAZE_LOCAL_DIR_AHEAD_LEFT:
			text[(*strlen)++]='A';
			text[(*strlen)++]='L';
			break;
		case MAZE_LOCAL_DIR_AHEAD_RIGHT:
			text[(*strlen)++]='A';
			text[(*strlen)++]='R';
			break;
		case MAZE_LOCAL_DIR_RIGHT:
			text[(*strlen)++]='R';
			text[(*strlen)++]=' ';
			break;
		case MAZE_LOCAL_DIR_LEFT:
			text[(*strlen)++]='L';
			text[(*strlen)++]=' ';
			break;
		case MAZE_LOCAL_DIR_BEHIND_LEFT:
			text[(*strlen)++]='B';
			text[(*strlen)++]='L';
			break;
		case MAZE_LOCAL_DIR_BEHIND_RIGHT:
			text[(*strlen)++]='B';
			text[(*strlen)++]='R';
			break;
		}
		text[(*strlen)++]='\t';

		//15 symboli na float

		(*strlen) += float_to_str(&(text[*strlen]), route->distance);
		text[(*strlen)++]='\t';

		//2 symbole + 2 koniec linii
		switch (route->global_direction) {
		case MAZE_GLOBAL_DIR_N:
			text[(*strlen)++]='N';
			text[(*strlen)++]=' ';
			break;
		case MAZE_GLOBAL_DIR_NE:
			text[(*strlen)++]='N';
			text[(*strlen)++]='E';
			break;
		case MAZE_GLOBAL_DIR_E:
			text[(*strlen)++]='E';
			text[(*strlen)++]=' ';
			break;
		case MAZE_GLOBAL_DIR_SE:
			text[(*strlen)++]='S';
			text[(*strlen)++]='E';
			break;
		case MAZE_GLOBAL_DIR_S:
			text[(*strlen)++]='S';
			text[(*strlen)++]=' ';
			break;
		case MAZE_GLOBAL_DIR_SW:
			text[(*strlen)++]='S';
			text[(*strlen)++]='W';
			break;
		case MAZE_GLOBAL_DIR_W:
			text[(*strlen)++]='W';
			text[(*strlen)++]=' ';
			break;
		case MAZE_GLOBAL_DIR_NW:
			text[(*strlen)++]='N';
			text[(*strlen)++]='W';
			break;
		}

		text[(*strlen)++]='\r';
		text[(*strlen)++]='\n';

	route=route->next;
	}
	text[(*strlen)]=0;
	return text;
}

/*
 * returns pointer on allocated string with map and its strlen
 */
char *draw_small_map_to_string(uint8_t (*map)[16], unsigned int *strlen){
	int8_t x, y;
	//bufor maks 596 + znak konca
	char *data=malloc_adapt(597);
	*strlen=0;

	//2 znaki
	data[(*strlen)++]='\r';
	data[(*strlen)++]='\n';
	//16*2+2 znaki
	y = 15;
	for (x = 0; x < 16; ++x) {
		data[(*strlen)++]=' ';
		if (maze_check_wall(map, x, y, MAZE_GLOBAL_DIR_N)) {
			data[(*strlen)++]='_';
		} else {
			data[(*strlen)++]=' ';
		}

	}
	data[(*strlen)++]='\r';
	data[(*strlen)++]='\n';

	//16*(2+16*2+1)
	for (y = 15; y >= 0; --y) {
		for (x = 0; x < 16; ++x) {
			if (maze_check_wall(map, x, y, MAZE_GLOBAL_DIR_W)) {
				data[(*strlen)++]='|';
			} else {
				data[(*strlen)++]=' ';
			}

			if (maze_check_wall(map, x, y, MAZE_GLOBAL_DIR_S)) {
				data[(*strlen)++]='_';
			} else {
				data[(*strlen)++]=' ';
			}

		}
		x = 15;
		if (maze_check_wall(map, x, y, MAZE_GLOBAL_DIR_E)) {
			data[(*strlen)++]='|';
		}
		data[(*strlen)++]='\r';
		data[(*strlen)++]='\n';

	}
	data[*strlen]=0;
	return data;
}

