/*
MIT License

Copyright (c) 2015-2018 Hubert Grzegorczyk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */


#include "math.h"
#include "stdlib.h"
#include "ctype.h"

//arm math

#include "kalman_maze.h"
#include "position_real.h"
#include "maze.h"



/*
 * Uk�ad wspolrzednych kalmana i labiryntu
 * N
 * ^y
 * |
 * |
 * |
 * |(0,0)
 * ------------> x
 * Punkt (0,0) znjaduje sie w srodku slupka
 */


void kalman_move_mean_predicted(kalman_maze_data *data, float speed_tran, float speed_rot, float deltaT){
	data->mean_predicted.x+=speed_tran*deltaT*cosf(data->mean_predicted.theta);

	data->mean_predicted.y+=speed_tran*deltaT*sinf(data->mean_predicted.theta);

	data->mean_predicted.theta+=speed_rot*deltaT;

	data->mean_predicted.theta=parse_angle_to_minus_pi_to_pi(data->mean_predicted.theta);
}

void kalman_set_position_to_known(kalman_maze_data *data, position_real known_pos){
	data->mean=known_pos;
	data->mean_predicted=known_pos;
	data->covariance.pData[0]=1;data->covariance.pData[1]=0;data->covariance.pData[2]=0;
	data->covariance.pData[3]=0;data->covariance.pData[4]=1;data->covariance.pData[5]=0;
	data->covariance.pData[6]=0;data->covariance.pData[7]=0;data->covariance.pData[8]=1;
}



predicted_sensor_meas kalman_get_predicted_meas_from_pred_rob_pos(kalman_parameters *params, uint8_t (*map)[16],
		position_real robotPredPos, position_real sensorPosInRobotFrame) {
	float sensorTheta = robotPredPos.theta + sensorPosInRobotFrame.theta;
	float sensorX = robotPredPos.x
			+ sensorPosInRobotFrame.x * cosf(robotPredPos.theta)
			- sensorPosInRobotFrame.y * sinf(robotPredPos.theta);
	float sensorY = robotPredPos.y
			+ sensorPosInRobotFrame.x * sinf(robotPredPos.theta)
			+ sensorPosInRobotFrame.y * cosf(robotPredPos.theta);

    sensorTheta=parse_angle_to_minus_pi_to_pi(sensorTheta);
    // mam kat -180 - 180 , 180 sie pokrywa (w radianach)
    //metry
    predicted_sensor_meas pred_horizontal_wall;
    pred_horizontal_wall.meas=4.0; //jesli nie moge policzyc nawet to musze tu ustawic niemozliwa wartosc
    pred_horizontal_wall.Ht_row[0]=0;
    pred_horizontal_wall.Ht_row[1]=0;
    pred_horizontal_wall.Ht_row[2]=0;
    pred_horizontal_wall.theta_meas.global_direction=0;

    predicted_sensor_meas pred_vertical_wall;
    pred_vertical_wall.meas=4.0; //jesli nie moge policzyc nawet to musze tu ustawic niemozliwa wartosc
    pred_vertical_wall.Ht_row[0]=0;
    pred_vertical_wall.Ht_row[1]=0;
    pred_vertical_wall.Ht_row[2]=0;
    pred_vertical_wall.theta_meas.global_direction=0;
    //sciany poziome - wzdluz x
    // sprawdzam czy linia czujnika nie idzie wzdluz scian
	if (((sensorTheta < M_PI / 180 && sensorTheta > -M_PI / 180)
			|| (sensorTheta > 179 * M_PI / 180
					|| sensorTheta < -179 * M_PI / 180)) == 0) {
		if(sensorTheta>0){ //czujnik patrzy w "gore"
			uint8_t a=0;
            while (0.174+a*0.180<sensorY)
                a+=1;
            // a zawiera wiersz w ktorym znajduje sie czujnik
			//szukam odleglosci od kolejnych wierszy scian i czy sciana na przecieciu z linia czujnika istnieje.
            // pierwsza znaleziona to nablizsza do czujnia pozioma sciana
            //p- point on the wall where beam from sensor ends (probably).
            float pY=0;
            float pX=0;
            uint8_t wallPositionYOnMazeMap=0;
            uint8_t wallPositionXOnMazeMap=0;
            uint8_t wallDetected=0;
            uint8_t sensorPositionXOnMazeMap=0;
            for(uint8_t wallNumberFromSensor=0;wallNumberFromSensor<params->search_box_size;++wallNumberFromSensor){
            	//wyznacz punkt przeciecia
                wallPositionYOnMazeMap=a+wallNumberFromSensor;
                if (wallPositionYOnMazeMap>15 || wallPositionYOnMazeMap<0)
                    break; //wyszedlem poza labirynt
                pY=0.174+(wallPositionYOnMazeMap)*0.180;
                pX=(1/tanf(sensorTheta))*(pY-sensorY)+sensorX;
                //sprawdz czy ta sciana istnieje (polnocna)
                wallPositionXOnMazeMap=floorf(pX/0.180);
                sensorPositionXOnMazeMap=floorf(sensorX/0.180);
                if (abs(sensorPositionXOnMazeMap-wallPositionXOnMazeMap)>=params->search_box_size)
                    break; //sciana przekracza kwadrat poszukiwan
                if (wallPositionXOnMazeMap<0 || wallPositionXOnMazeMap>15)
                    break; //wyszedlem poza labirynt
                if (!maze_check_if_wall_discovered(map,wallPositionXOnMazeMap,wallPositionYOnMazeMap,MAZE_GLOBAL_DIR_N))
                	break; //ppromien przechodzi przez nieokdryta sciane, jest ryzyko ze ona tam jest i czujnik moze mierzyc do niej
                float local_distance_from_wall_edge = params->distance_from_wall_edge;
                if (wallNumberFromSensor > 0)
                {
                	local_distance_from_wall_edge = params->distance_from_wall_edge_further_walls;
                }
				if (maze_check_wall(map,wallPositionXOnMazeMap,wallPositionYOnMazeMap,MAZE_GLOBAL_DIR_N)) {
					if(fabsf(wallPositionXOnMazeMap*0.180-pX)<local_distance_from_wall_edge || fabsf((wallPositionXOnMazeMap+1)*0.180-pX)<local_distance_from_wall_edge){
						wallDetected=0;
					}else{
						wallDetected = 1;
					}
					break;
				}
            }
            if (wallDetected){
                pred_horizontal_wall.meas=sqrtf(powf(pX-sensorX,2)+powf(pY-sensorY,2));
                float y=robotPredPos.y;
                float theta=robotPredPos.theta;
                float w_y=pY;
                float alph=sensorPosInRobotFrame.theta;
                float a=sensorPosInRobotFrame.x;
                float d=sensorPosInRobotFrame.y;
                float d_dy=-(4*y - 4*w_y + 4*d*cosf(theta) + 4*a*sinf(theta))/(2*(cosf(2*alph + 2*theta) - 1)*sqrtf(((powf(tanf(alph + theta),2) + 1)*powf((y - w_y + d*cosf(theta) + a*sinf(theta)),2))/powf(tanf(alph + theta),2)));
                float d_dtheta=((powf(tanf(alph + theta),2) + 1)*(w_y*cosf(alph + theta) - y*cosf(alph + theta) - d*cosf(alph) + a*sinf(alph))*(y - w_y + d*cosf(theta) + a*sinf(theta)))/(cosf(alph + theta)*powf(tanf(alph + theta),3)*sqrtf(((powf(tanf(alph + theta),2) + 1)*powf((y - w_y + d*cosf(theta) + a*sinf(theta)),2))/powf(tanf(alph + theta),2)));
				pred_horizontal_wall.Ht_row[0]=0;
                pred_horizontal_wall.Ht_row[1]=d_dy;
                pred_horizontal_wall.Ht_row[2]=d_dtheta;
                pred_horizontal_wall.theta_meas.global_direction=1;
                pred_horizontal_wall.theta_meas.w_cord=w_y;
            }
		} else {
			uint8_t a=0;
            while (0.006+a*0.180<sensorY-0.180)
                a+=1;
            // a zawiera wiersz w ktorym znajduje sie czujnik
			//szukam odleglosci od kolejnych wierszy scian i czy sciana na przecieciu z linia czujnika istnieje.
            // pierwsza znaleziona to nablizsza do czujnia pozioma sciana
            float pY=0;
            float pX=0;
            uint8_t wallPositionYOnMazeMap=0;
            uint8_t wallPositionXOnMazeMap=0;
            uint8_t wallDetected=0;
            uint8_t sensorPositionXOnMazeMap=0;
            for(uint8_t wallNumberFromSensor=0;wallNumberFromSensor<params->search_box_size;++wallNumberFromSensor){
            	//wyznacz punkt przeciecia
                wallPositionYOnMazeMap=a-wallNumberFromSensor;
                if (wallPositionYOnMazeMap>15 || wallPositionYOnMazeMap<0)
                    break; //wyszedlem poza labirynt
                pY=0.006+(wallPositionYOnMazeMap)*0.180;
                pX=(1/tanf(sensorTheta))*(pY-sensorY)+sensorX;
                //sprawdz czy ta sciana istnieje (polnocna)
                wallPositionXOnMazeMap=floorf(pX/0.180);
                sensorPositionXOnMazeMap=floorf(sensorX/0.180);
                if (abs(sensorPositionXOnMazeMap-wallPositionXOnMazeMap)>=params->search_box_size)
                    break; //sciana przekracza kwadrat poszukiwan
                if (wallPositionXOnMazeMap<0 || wallPositionXOnMazeMap>15)
                    break; //wyszedlem poza labirynt
                if (!maze_check_if_wall_discovered(map,wallPositionXOnMazeMap,wallPositionYOnMazeMap,MAZE_GLOBAL_DIR_S))
                	break; //ppromien przechodzi przez nieokdryta sciane, jest ryzyko ze ona tam jest i czujnik moze mierzyc do niej
                float local_distance_from_wall_edge = params->distance_from_wall_edge;
                if (wallNumberFromSensor > 0)
                {
                	local_distance_from_wall_edge = params->distance_from_wall_edge_further_walls;
                }
				if (maze_check_wall(map,wallPositionXOnMazeMap,wallPositionYOnMazeMap,MAZE_GLOBAL_DIR_S)) {
					if(fabsf(wallPositionXOnMazeMap*0.180-pX)<local_distance_from_wall_edge || fabsf((wallPositionXOnMazeMap+1)*0.180-pX)<local_distance_from_wall_edge){
						wallDetected=0;
					}else{
						wallDetected = 1;
					}
					break;
				}
            }
            if (wallDetected){
                pred_horizontal_wall.meas=sqrtf(powf(pX-sensorX,2)+powf(pY-sensorY,2));
                float y=robotPredPos.y;
                float theta=robotPredPos.theta;
                float w_y=pY;
                float alph=sensorPosInRobotFrame.theta;
                float a=sensorPosInRobotFrame.x;
                float d=sensorPosInRobotFrame.y;
                float d_dy=-(4*y - 4*w_y + 4*d*cosf(theta) + 4*a*sinf(theta))/(2*(cosf(2*alph + 2*theta) - 1)*sqrtf(((powf(tanf(alph + theta),2) + 1)*powf((y - w_y + d*cosf(theta) + a*sinf(theta)),2))/powf(tanf(alph + theta),2)));
                float d_dtheta=((powf(tanf(alph + theta),2) + 1)*(w_y*cosf(alph + theta) - y*cosf(alph + theta) - d*cosf(alph) + a*sinf(alph))*(y - w_y + d*cosf(theta) + a*sinf(theta)))/(cosf(alph + theta)*powf(tanf(alph + theta),3)*sqrtf(((powf(tanf(alph + theta),2) + 1)*powf((y - w_y + d*cosf(theta) + a*sinf(theta)),2))/powf(tanf(alph + theta),2)));
				pred_horizontal_wall.Ht_row[0]=0;
                pred_horizontal_wall.Ht_row[1]=d_dy;
                pred_horizontal_wall.Ht_row[2]=d_dtheta;
                pred_horizontal_wall.theta_meas.global_direction=3;
                pred_horizontal_wall.theta_meas.w_cord=w_y;
            }
		}

	}

	///SCIANY PIONOWE
	if (((sensorTheta < 91 * M_PI / 180 && sensorTheta > 89 * M_PI / 180)
			|| (sensorTheta < -89 * M_PI / 180 && sensorTheta > -91 * M_PI / 180))
			== 0) {
		if ((sensorTheta > M_PI / 2 || sensorTheta < -M_PI / 2)) { //patrze w lewo
			uint8_t a = 0;
			while (0.006 + a * 0.180 < sensorX - 0.180)
				a += 1;
			// a zawiera kolumne w ktorej znajduje sie czujnik
			// szukam odleglosci od kolejnych kolumn scian i czy sciana na przecieciu z linia czujnika istnieje.
			// pierwsza znaleziona to nablizsza do czujnia pionowa sciana
			float pY = 0;
			float pX = 0;
			uint8_t wallPositionYOnMazeMap = 0;
			uint8_t wallPositionXOnMazeMap = 0;
			uint8_t wallDetected = 0;
			uint8_t sensorPositionYOnMazeMap = 0;
			for (uint8_t wallNumberFromSensor = 0;
					wallNumberFromSensor < params->search_box_size;
					++wallNumberFromSensor) {
				//wyznacz punkt przeciecia
				wallPositionXOnMazeMap = a - wallNumberFromSensor;
				if (wallPositionXOnMazeMap < 0 || wallPositionXOnMazeMap > 15)
					break; //wyszedlem poza labirynt
				pX = 0.006 + (wallPositionXOnMazeMap) * 0.180;
				pY = (tanf(sensorTheta)) * (pX - sensorX) + sensorY;
				//sprawdz czy ta sciana istnieje (zachodnia)
				wallPositionYOnMazeMap = floorf(pY / 0.180);
				sensorPositionYOnMazeMap = floorf(sensorY / 0.180);
				if (abs(
						sensorPositionYOnMazeMap
								- wallPositionYOnMazeMap)>=params->search_box_size)
					break; //sciana przekracza kwadrat poszukiwan
				if (wallPositionYOnMazeMap < 0 || wallPositionYOnMazeMap > 15)
					break; //wyszedlem poza labirynt
                if (!maze_check_if_wall_discovered(map,wallPositionXOnMazeMap,wallPositionYOnMazeMap,MAZE_GLOBAL_DIR_W))
                	break; //ppromien przechodzi przez nieokdryta sciane, jest ryzyko ze ona tam jest i czujnik moze mierzyc do niej
				//czy jest lewa sciana

                float local_distance_from_wall_edge = params->distance_from_wall_edge;
                if (wallNumberFromSensor > 0)
                {
                	local_distance_from_wall_edge = params->distance_from_wall_edge_further_walls;
                }
				if (maze_check_wall(map,wallPositionXOnMazeMap,wallPositionYOnMazeMap,MAZE_GLOBAL_DIR_W)) {
					if(fabsf(wallPositionYOnMazeMap*0.180-pY)<local_distance_from_wall_edge || fabsf((wallPositionYOnMazeMap+1)*0.180-pY)<local_distance_from_wall_edge){
						wallDetected=0;
					}else{
						wallDetected = 1;
					}
					break;
				}
			}
			if (wallDetected){
				pred_vertical_wall.meas = sqrtf(
						powf(pX - sensorX, 2) + powf(pY - sensorY, 2));
				float x = robotPredPos.x;
				float theta = robotPredPos.theta; //robota
				float w_x = pX;
				float alph = sensorPosInRobotFrame.theta;
				//kat czujnika od osi x robota
				float a = sensorPosInRobotFrame.x;
				float d=sensorPosInRobotFrame.y;
			    float d_dx=-(4*w_x - 4*x - 4*a*cosf(theta) + 4*d*sinf(theta))/(2*sqrtf((powf(tanf(alph + theta),2) + 1)*powf((w_x - x - a*cosf(theta) + d*sinf(theta)),2))*(cosf(2*alph + 2*theta) + 1));
			    float d_dtheta=((powf(tanf(alph + theta),2) + 1)*(w_x*sinf(alph + theta) - x*sinf(alph + theta) + d*cosf(alph) - a*sinf(alph))*(w_x - x - a*cosf(theta) + d*sinf(theta)))/(cosf(alph + theta)*sqrtf((powf(tanf(alph + theta),2) + 1)*powf((w_x - x - a*cosf(theta) + d*sinf(theta)),2)));
				pred_vertical_wall.Ht_row[0]=d_dx;
			    pred_vertical_wall.Ht_row[1]=0;
			    pred_vertical_wall.Ht_row[2]=d_dtheta;
			    pred_vertical_wall.theta_meas.global_direction=4;
			    pred_vertical_wall.theta_meas.w_cord=w_x;
			}
		}else{//patrze w "prawo"
			uint8_t a = 0;
			while (0.174+a*0.180<sensorX)
				a += 1;
			// a zawiera kolumne w ktorej znajduje sie czujnik
			// szukam odleglosci od kolejnych kolumn scian i czy sciana na przecieciu z linia czujnika istnieje.
			// pierwsza znaleziona to nablizsza do czujnia pionowa sciana
			float pY = 0;
			float pX = 0;
			uint8_t wallPositionYOnMazeMap = 0;
			uint8_t wallPositionXOnMazeMap = 0;
			uint8_t wallDetected = 0;
			uint8_t sensorPositionYOnMazeMap = 0;
			for (uint8_t wallNumberFromSensor = 0;
					wallNumberFromSensor < params->search_box_size;
					++wallNumberFromSensor) {
				//wyznacz punkt przeciecia
				wallPositionXOnMazeMap = a + wallNumberFromSensor;
				if (wallPositionXOnMazeMap < 0 || wallPositionXOnMazeMap > 15)
					break; //wyszedlem poza labirynt
				pX = 0.174+(wallPositionXOnMazeMap)*0.180;
				pY = (tanf(sensorTheta)) * (pX - sensorX) + sensorY;
				//sprawdz czy ta sciana istnieje (zachodnia)
				wallPositionYOnMazeMap = floorf(pY / 0.180);
				sensorPositionYOnMazeMap = floorf(sensorY / 0.180);
				if (abs(
						sensorPositionYOnMazeMap
								- wallPositionYOnMazeMap)>=params->search_box_size)
					break; //sciana przekracza kwadrat poszukiwan
				if (wallPositionYOnMazeMap < 0 || wallPositionYOnMazeMap > 15)
					break; //wyszedlem poza labirynt
                if (!maze_check_if_wall_discovered(map,wallPositionXOnMazeMap,wallPositionYOnMazeMap,MAZE_GLOBAL_DIR_E))
                	break; //ppromien przechodzi przez nieokdryta sciane, jest ryzyko ze ona tam jest i czujnik moze mierzyc do niej
				//czy jest prawa sciana
                float local_distance_from_wall_edge = params->distance_from_wall_edge;
                if (wallNumberFromSensor > 0)
                {
                	local_distance_from_wall_edge = params->distance_from_wall_edge_further_walls;
                }
				if (maze_check_wall(map,wallPositionXOnMazeMap,wallPositionYOnMazeMap,MAZE_GLOBAL_DIR_E)) {
					if(fabsf(wallPositionYOnMazeMap*0.180-pY)<local_distance_from_wall_edge || fabsf((wallPositionYOnMazeMap+1)*0.180-pY)<local_distance_from_wall_edge){
						wallDetected=0;
					}else{
						wallDetected = 1;
					}
					break;
				}
			}
			if (wallDetected){
				pred_vertical_wall.meas = sqrtf(
						powf(pX - sensorX, 2) + powf(pY - sensorY, 2));
				float x = robotPredPos.x;
				float theta = robotPredPos.theta; //robota
				float w_x = pX;
				float alph = sensorPosInRobotFrame.theta;
				//kat czujnika od osi x robota
				float a = sensorPosInRobotFrame.x;
				float d=sensorPosInRobotFrame.y;
			    float d_dx=-(4*w_x - 4*x - 4*a*cosf(theta) + 4*d*sinf(theta))/(2*sqrtf((powf(tanf(alph + theta),2) + 1)*powf((w_x - x - a*cosf(theta) + d*sinf(theta)),2))*(cosf(2*alph + 2*theta) + 1));
			    float d_dtheta=((powf(tanf(alph + theta),2) + 1)*(w_x*sinf(alph + theta) - x*sinf(alph + theta) + d*cosf(alph) - a*sinf(alph))*(w_x - x - a*cosf(theta) + d*sinf(theta)))/(cosf(alph + theta)*sqrtf((powf(tanf(alph + theta),2) + 1)*powf((w_x - x - a*cosf(theta) + d*sinf(theta)),2)));
				pred_vertical_wall.Ht_row[0]=d_dx;
			    pred_vertical_wall.Ht_row[1]=0;
			    pred_vertical_wall.Ht_row[2]=d_dtheta;
			    pred_vertical_wall.theta_meas.global_direction=2;
			    pred_vertical_wall.theta_meas.w_cord=w_x;
			}

		}

	}

	if(pred_vertical_wall.meas<pred_horizontal_wall.meas){
		return pred_vertical_wall;
	}else{
		return pred_horizontal_wall;
	}

}

void kalman_delete_hist(kalman_maze_data *data){
	for(uint8_t x=0;x<10;++x){
		data->left_vl_hist[x].global_direction=0;
		data->right_vl_hist[x].global_direction=0;
	}
}

theta_meas_data kalman_get_cord_from_meas(kalman_parameters *params, uint8_t (*map)[16],sensor_meas_snapshot current,position_real sensorPosInRobotFrame){

	predicted_sensor_meas curr_predicted=kalman_get_predicted_meas_from_pred_rob_pos(params, map,current.predicted_pos_at_meas_time,sensorPosInRobotFrame);

	if(fabsf(curr_predicted.meas-current.meas) > 0.05){
		curr_predicted.theta_meas.global_direction=0;
		return curr_predicted.theta_meas;
	}

    //ograniczyc katy zeby nie liczyl zawsze tylko w jakmis zakresie jak jade w danym kierunku
	//kiedy jade w prawo nie moge patrzec w prawo w tym pomiarze
	float robot_theta=current.predicted_pos_at_meas_time.theta;
	//kierunek +-60 stopni (pokrywaja sie dla 45 (ukosow))
	if((robot_theta<M_PI/3 && robot_theta>-M_PI/3) || (robot_theta>2*M_PI/3 && robot_theta<-2*M_PI/3 )){ //w prawo jade lub w lewo jade
		if(curr_predicted.theta_meas.global_direction==2 || curr_predicted.theta_meas.global_direction==4){
			curr_predicted.theta_meas.global_direction = 0;
			return curr_predicted.theta_meas;
		}
	}

	if((robot_theta>M_PI/6 && robot_theta<5*M_PI/6 ) || (robot_theta<-M_PI/6 && robot_theta>-5*M_PI/6)){ //w gore jade (+y) lub dol (-y)
		if(curr_predicted.theta_meas.global_direction==1 || curr_predicted.theta_meas.global_direction==3){
			curr_predicted.theta_meas.global_direction = 0;
			return curr_predicted.theta_meas;
		}
	}

    //patzre w prawo    // patrze w lewo
    if (curr_predicted.theta_meas.global_direction==2 || curr_predicted.theta_meas.global_direction==4 ){
    	curr_predicted.theta_meas.w_cord=curr_predicted.theta_meas.w_cord-sensorPosInRobotFrame.x*cosf(current.predicted_pos_at_meas_time.theta)+sensorPosInRobotFrame.y*sinf(current.predicted_pos_at_meas_time.theta)-current.meas*cosf(current.predicted_pos_at_meas_time.theta+sensorPosInRobotFrame.theta);
    	curr_predicted.theta_meas.another_cord=current.predicted_pos_at_meas_time.y;
    }

    //patrze w gore //patrze w dol
    if (curr_predicted.theta_meas.global_direction==1 || curr_predicted.theta_meas.global_direction==3 ){
    	curr_predicted.theta_meas.w_cord=curr_predicted.theta_meas.w_cord-sensorPosInRobotFrame.x*sinf(current.predicted_pos_at_meas_time.theta)-sensorPosInRobotFrame.y*cosf(current.predicted_pos_at_meas_time.theta)-current.meas*sinf(current.predicted_pos_at_meas_time.theta+sensorPosInRobotFrame.theta);
    	curr_predicted.theta_meas.another_cord=current.predicted_pos_at_meas_time.x;
    }
    //teraz w curr_predicted.theta_meas.w_cord mam pozycje x/y robota w globalnym ukladzie, a global dir oznacza czy jest ok (0 nie ok) i gdzie patrzy czujnik
    return curr_predicted.theta_meas;
}

uint8_t kalman_measure_theta_from_two_meas(kalman_parameters *params, theta_meas_data prev,theta_meas_data curr,float *theta){
    //poprzedni pomiar
	*theta=0;

	if(prev.global_direction!=curr.global_direction){
		return 1;
	}

	//pozycje robota obliczone na podstawie �ciany i drugiej wspolrzednej z predict pos
	//jesli za blisko siebie sa punkty
    if (sqrtf(powf(curr.another_cord-prev.another_cord,2.0)+powf(curr.w_cord-prev.w_cord,2.0)) < params->theta_meas_min_dist_between_2_meas)  {
       return 1;
    }

	//w cord to y
	if((prev.global_direction==1 && curr.global_direction==1) ||(prev.global_direction==3 && curr.global_direction==3) ){

		*theta=atan2f(curr.w_cord-prev.w_cord,curr.another_cord-prev.another_cord);

	}else if((prev.global_direction==2 && curr.global_direction==2) ||(prev.global_direction==4 && curr.global_direction==4)){
		*theta=atan2f(curr.another_cord-prev.another_cord,curr.w_cord-prev.w_cord);
	}else{
		return 1;
	}

    return 0;
}

void kalman_delta_pos_meas_on_off(kalman_maze_data *data,int8_t state){
	if(state){
		kalman_delete_hist(data);
		data->delta_pos_meas_on=1;
	}else{
		data->delta_pos_meas_on=0;
	}
}

void kalman_init_data(kalman_maze_data *data,position_real start_pos){

//nie licze tutaj meanPredict tylko w speed_profile poniewaz mam wieksza granulacje i dokladnosc dzieki temu - ale nie brac
	//z enkoderow tylko ze sterowan!!! to jest zle!
	//TODO z enkoderow to moze byc kolejny pomiar. Sterowania sa inne
	//TODO chyba musze tu zakladac mutex bo jednak moze byc  niefajnie jak w polowie cos odczytam
	//position_real meanPredict=mean_1+to co nacalkowalem ze sterowan;
	//pData[i*numCols + j] - jak wyglada macierz, i-wiersz, j - kolumna

	data->mean=start_pos;
	data->mean_predicted=start_pos;

	//alokacja zmiennych
	data->Gt.numRows=3;
	data->Gt.numCols=3;
	data->Gt.pData=data->Gt_data;

	data->Rt.numRows=3;
	data->Rt.numCols=3;
	data->Rt.pData=data->Rt_data;
	data->Rt.pData[0]=data->params.var_x;data->Rt.pData[1]=0;data->Rt.pData[2]=0;
	data->Rt.pData[3]=0;data->Rt.pData[4]=data->params.var_y;data->Rt.pData[5]=0;
	data->Rt.pData[6]=0;data->Rt.pData[7]=0;data->Rt.pData[8]=data->params.var_theta;

	data->Gt_tran.numRows=3;
	data->Gt_tran.numCols=3;
	data->Gt_tran.pData=data->Gt_tran_data;

	//answer ma pola dla 5x5
	data->ans.numRows=3;
	data->ans.numCols=3;
	data->ans.pData=data->ans_data;

	data->covariance.numRows=3;
	data->covariance.numCols=3;
	data->covariance.pData=data->covariance_data;
	data->covariance.pData[0]=1;data->covariance.pData[1]=0;data->covariance.pData[2]=0;
	data->covariance.pData[3]=0;data->covariance.pData[4]=1;data->covariance.pData[5]=0;
	data->covariance.pData[6]=0;data->covariance.pData[7]=0;data->covariance.pData[8]=1;

	data->covariance_predict.numRows=3;
	data->covariance_predict.numCols=3;
	data->covariance_predict.pData=data->covariance_predict_data;

	data->Ht.numRows=5;
	data->Ht.numCols=3;
	data->Ht.pData=data->Ht_data;

	data->Ht_tran.numRows=3;
	data->Ht_tran.numCols=5;
	data->Ht_tran.pData=data->Ht_tran_data;

	data->Qt.numRows=5;
	data->Qt.numCols=5;
	data->Qt.pData=data->Qt_data;
	data->Qt.pData[0]=data->params.var_vl;data->Qt.pData[1]=0;data->Qt.pData[2]=0;data->Qt.pData[3]=0;data->Qt.pData[4]=0;
	data->Qt.pData[5]=0;data->Qt.pData[6]=data->params.var_vl;data->Qt.pData[7]=0;data->Qt.pData[8]=0;data->Qt.pData[9]=0;
	data->Qt.pData[10]=0;data->Qt.pData[11]=0;data->Qt.pData[12]=data->params.var_vl;data->Qt.pData[13]=0;data->Qt.pData[14]=0;
	data->Qt.pData[15]=0;data->Qt.pData[16]=0;data->Qt.pData[17]=0;data->Qt.pData[18]=data->params.var_theta_meas;data->Qt.pData[19]=0;
	data->Qt.pData[20]=0;data->Qt.pData[21]=0;data->Qt.pData[22]=0;data->Qt.pData[23]=0;data->Qt.pData[24]=data->params.var_theta_meas;

	//tak samo moze byc 5x5
	data->ans2.numRows=3;
	data->ans2.numCols=3;
	data->ans2.pData=data->ans2_data;

	//tak samo moze byc 5x5
	data->Kt.numRows=3;
	data->Kt.numCols=5;
	data->Kt.pData=data->Kt_data;

	data->delta_zt_zt_predict.numRows=5;
	data->delta_zt_zt_predict.numCols=1;
	data->delta_zt_zt_predict.pData=data->delta_zt_data;

	data->ansVector.numRows=3;
	data->ansVector.numCols=1;
	data->ansVector.pData=data->ans_vector_data;

	//To sie nie zmienia
	data->identity_3.numRows=3;
	data->identity_3.numCols=3;
	data->identity_3.pData=data->identity_3_data;
	data->identity_3.pData[0]=1.0;data->identity_3.pData[1]=0;data->identity_3.pData[2]=0;
	data->identity_3.pData[3]=0;data->identity_3.pData[4]=1.0;data->identity_3.pData[5]=0;
	data->identity_3.pData[6]=0;data->identity_3.pData[7]=0;data->identity_3.pData[8]=1.0;


	data->sensors_meas[0].new_meas=0;
	data->sensors_meas[1].new_meas=0;
	data->sensors_meas[2].new_meas=0;

	data->index_left=0;
	data->index_right=0;

	for(uint8_t x=0;x<10;++x){
		data->left_vl_hist[x].global_direction=0;
		data->right_vl_hist[x].global_direction=0;
	}

	data->delta_pos_meas_on=0;

}


void kalman_step(kalman_maze_data *data,float speed_tran,float deltaT,uint8_t (*map)[16]){

	data->mean_predicted.theta=parse_angle_to_minus_pi_to_pi(data->mean_predicted.theta);

	data->Gt.pData[0]=1;data->Gt.pData[1]=0;data->Gt.pData[2]=-speed_tran*arm_sin_f32(data->mean.theta)*deltaT;
	data->Gt.pData[3]=0;data->Gt.pData[4]=1;data->Gt.pData[5]=speed_tran*arm_cos_f32(data->mean.theta)*deltaT;
	data->Gt.pData[6]=0;data->Gt.pData[7]=0;data->Gt.pData[8]=1;

	arm_mat_trans_f32(&(data->Gt),&(data->Gt_tran));

	arm_mat_mult_f32(&data->Gt,&data->covariance,&data->ans);
	arm_mat_mult_f32(&data->ans,&data->Gt_tran,&data->covariance);
	arm_mat_add_f32(&data->covariance,&data->Rt,&data->covariance_predict);

	predicted_sensor_meas vl_pred_left;
	predicted_sensor_meas vl_pred_front;
	predicted_sensor_meas vl_pred_right;
	//TUTAJ MAGIA Z VL
	//wyznaczyc wiersze Ht i przewidywane pomiary
	vl_pred_left = kalman_get_predicted_meas_from_pred_rob_pos(&(data->params), map,
			data->sensors_meas[0].predicted_pos_at_meas_time,
			data->params.sensors_position_in_robot_frame[0]);
	vl_pred_front = kalman_get_predicted_meas_from_pred_rob_pos(&(data->params), map,
			data->sensors_meas[1].predicted_pos_at_meas_time,
			data->params.sensors_position_in_robot_frame[1]);
	vl_pred_right = kalman_get_predicted_meas_from_pred_rob_pos(&(data->params), map,
			data->sensors_meas[2].predicted_pos_at_meas_time,
			data->params.sensors_position_in_robot_frame[2]);


	theta_meas_data temp_data;
	float theta1;
	float theta2;
	int temp_index;


	data->Ht.pData[9]=0;
	data->Ht.pData[10]=0;
	data->Ht.pData[11]=0;
	if(data->sensors_meas[0].new_meas && data->delta_pos_meas_on){
		temp_data=kalman_get_cord_from_meas(&(data->params), map,data->sensors_meas[0],data->params.sensors_position_in_robot_frame[0]);
		if(temp_data.global_direction!=0){
			//dodaje do listy i probuje policzyc thete
			++(data->index_left);
			if(data->index_left>=10){
				data->index_left=0;
			}
			data->left_vl_hist[data->index_left]=temp_data;
			temp_index=data->index_left;

			temp_index-=data->params.theta_meas_gap;
			if(temp_index<0){
				temp_index+=10;
			}

			if(kalman_measure_theta_from_two_meas(&(data->params), data->left_vl_hist[temp_index],data->left_vl_hist[data->index_left],&theta1)){

			}else{
				data->Ht.pData[9]=0;
				data->Ht.pData[10]=0;
				data->Ht.pData[11]=1;
			}

		}
	}


	data->Ht.pData[12]=0;
	data->Ht.pData[13]=0;
	data->Ht.pData[14]=0;
	if(data->sensors_meas[2].new_meas && data->delta_pos_meas_on){
		temp_data=kalman_get_cord_from_meas(&(data->params), map,data->sensors_meas[2],data->params.sensors_position_in_robot_frame[2]);
		if(temp_data.global_direction!=0){
			//dodaje do listy i probuje policzyc thete
			++(data->index_right);
			if(data->index_right>=10){
				data->index_right=0;
			}
			data->right_vl_hist[data->index_right]=temp_data;
			temp_index=data->index_right;

			temp_index-=data->params.theta_meas_gap;
			if(temp_index<0){
				temp_index+=10;
			}

			if(kalman_measure_theta_from_two_meas(&(data->params), data->right_vl_hist[temp_index],data->right_vl_hist[data->index_right],&theta2)){

			}else{
				data->Ht.pData[12]=0;
				data->Ht.pData[13]=0;
				data->Ht.pData[14]=1;
			}

		}
	}



	//TODO pomiar odpowiedni z tego samego momentu co wyzej
	data->delta_zt_zt_predict.pData[0]=data->sensors_meas[0].meas-vl_pred_left.meas;
	data->delta_zt_zt_predict.pData[1]=data->sensors_meas[1].meas-vl_pred_front.meas;
	data->delta_zt_zt_predict.pData[2]=data->sensors_meas[2].meas-vl_pred_right.meas;
	data->delta_zt_zt_predict.pData[3]=angular_distance( data->mean_predicted.theta,theta1);
	data->delta_zt_zt_predict.pData[4]=angular_distance( data->mean_predicted.theta,theta2);

	if (fabsf(data->delta_zt_zt_predict.pData[0]) > data->params.vl_predict_meas_allowed_delta
			|| data->sensors_meas[0].new_meas == 0) {
		data->Ht.pData[0] = 0;
		data->Ht.pData[1] = 0;
		data->Ht.pData[2] = 0;
	} else {
		data->Ht.pData[0] = vl_pred_left.Ht_row[0];
		data->Ht.pData[1] = vl_pred_left.Ht_row[1];
		data->Ht.pData[2] = vl_pred_left.Ht_row[2];
	}
	data->sensors_meas[0].new_meas=0;

	if (fabsf(data->delta_zt_zt_predict.pData[1]) > data->params.vl_predict_meas_allowed_delta || data->sensors_meas[1].new_meas == 0) {
		data->Ht.pData[3] = 0;
		data->Ht.pData[4] = 0;
		data->Ht.pData[5] = 0;
	} else {
		data->Ht.pData[3] = vl_pred_front.Ht_row[0];
		data->Ht.pData[4] = vl_pred_front.Ht_row[1];
		data->Ht.pData[5] = vl_pred_front.Ht_row[2];
	}
	data->sensors_meas[1].new_meas=0;

	if (fabsf(data->delta_zt_zt_predict.pData[2]) > data->params.vl_predict_meas_allowed_delta
			|| data->sensors_meas[2].new_meas == 0) {
		data->Ht.pData[6] = 0;
		data->Ht.pData[7] = 0;
		data->Ht.pData[8] = 0;
	} else {
		data->Ht.pData[6] = vl_pred_right.Ht_row[0];
		data->Ht.pData[7] = vl_pred_right.Ht_row[1];
		data->Ht.pData[8] = vl_pred_right.Ht_row[2];
	}
	data->sensors_meas[2].new_meas=0;

	/////
	arm_mat_trans_f32(&data->Ht,&data->Ht_tran);

	//WYLICZANIE Kt
	data->ans.numRows=5;
	data->ans.numCols=3;
	arm_mat_mult_f32(&data->Ht,&data->covariance_predict,&data->ans);
	//mam wynik posredni w Kt
	data->Kt.numRows=5;
	data->Kt.numCols=5;
	arm_mat_mult_f32(&data->ans,&data->Ht_tran,&data->Kt);
	data->ans2.numRows=5;
	data->ans2.numCols=5;
	arm_mat_add_f32(&data->Kt,&data->Qt,&data->ans2);
	data->ans.numRows=5;
	data->ans.numCols=5;
	arm_mat_inverse_f32(&data->ans2,&data->ans); //odwracam mam w ans
	data->ans2.numRows=3;
	data->ans2.numCols=5;
	arm_mat_mult_f32(&data->Ht_tran,&data->ans,&data->ans2); //teraz od prawej strony
	data->Kt.numRows=3;
	data->Kt.numCols=5;
	arm_mat_mult_f32(&data->covariance_predict,&data->ans2,&data->Kt); //wynik

	//aktualizacja
	//zebrac odpowiednie part pomiarow i przewidywanych pomiarow

	//mean
	arm_mat_mult_f32(&data->Kt,&data->delta_zt_zt_predict,&data->ansVector);
	//dodac z ans do mean
	data->mean.x=data->mean_predicted.x+data->ansVector.pData[0];
	data->mean.y=data->mean_predicted.y+data->ansVector.pData[1];
	data->mean.theta=data->mean_predicted.theta+data->ansVector.pData[2];

	data->mean.theta=parse_angle_to_minus_pi_to_pi(data->mean.theta);

	//covariance
	data->ans.numRows=3;
	data->ans.numCols=3;
	arm_mat_mult_f32(&data->Kt,&data->Ht,&data->ans);
	data->ans2.numRows=3;
	data->ans2.numCols=3;
	arm_mat_sub_f32(&data->identity_3,&data->ans,&data->ans2);
	arm_mat_mult_f32(&data->ans2,&data->covariance_predict,&data->covariance);

	//dla nastepnej iteracji
	data->mean_predicted=data->mean;
}

