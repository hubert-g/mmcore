/*
MIT License

Copyright (c) 2015-2018 Hubert Grzegorczyk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#ifndef APPLICATION_USER_LABIRYNT_H_
#define APPLICATION_USER_LABIRYNT_H_

#include "stdint.h"

/*
 * N
 * ^ y
 * |
 * |
 * |
 * |
 * |
 * ----------------> x E
 * (0,0)
 *
 */

typedef enum {
	MAZE_GLOBAL_DIR_N = 0b1000,
	MAZE_GLOBAL_DIR_NE = 0b1100,
	MAZE_GLOBAL_DIR_E = 0b0100,
	MAZE_GLOBAL_DIR_SE = 0b0110,
	MAZE_GLOBAL_DIR_S = 0b0010,
	MAZE_GLOBAL_DIR_SW = 0b0011,
	MAZE_GLOBAL_DIR_W = 0b0001,
	MAZE_GLOBAL_DIR_NW = 0b1001
} maze_global_dirs;


typedef struct {
	uint8_t x;
	uint8_t y;
	maze_global_dirs course;
} maze_position;


typedef struct{
	uint8_t map[16][16];
	uint8_t floodfill[16][16];
	maze_position cur_pos;
}maze_data;

/*
 * You can use directions beetwen like AHEAD+RIGHT
 */
typedef enum {
	MAZE_LOCAL_DIR_AHEAD = 0b0001,
	MAZE_LOCAL_DIR_AHEAD_RIGHT = 0b0011,
	MAZE_LOCAL_DIR_AHEAD_LEFT = 0b0101,
	MAZE_LOCAL_DIR_RIGHT = 0b0010,
	MAZE_LOCAL_DIR_LEFT = 0b0100,
	MAZE_LOCAL_DIR_BEHIND = 0b1000,
	MAZE_LOCAL_DIR_BEHIND_LEFT = 0b1100,
	MAZE_LOCAL_DIR_BEHIND_RIGHT = 0b1010
} maze_local_dirs;

typedef enum {
	MAZE_MAP_BIT_WALL_W,
	MAZE_MAP_BIT_WALL_S,
	MAZE_MAP_BIT_WALL_E,
	MAZE_MAP_BIT_WALL_N,
	MAZE_MAP_BIT_VISITED,
} maze_map_bits;



struct maze_route_list {
	maze_local_dirs direction; //direction to move from actual square
	maze_global_dirs global_direction; //direction in global view
	float distance; //distance in m, for turns is b (turn size)
	struct maze_route_list* next;

};


void maze_init_data(maze_data *data,uint8_t start_x,uint8_t start_y,maze_global_dirs start_dir);

/*
 * Returned values
 * 0 - generated next move
 * 1 - target achieved
 * 2 - no way to target
 */
uint8_t maze_searching_get_next_move_to_target(maze_data *data,
		uint8_t left_wall_on_cur_pos, uint8_t right_wall_on_cur_pos,
		uint8_t front_wall_on_cur_pos,uint8_t target_x, uint8_t target_y ,maze_local_dirs *next_move);

/*
 * Returned values
 * 0 - route generated
 * 1 - no way to target
 * 2 - route is through not visited squares
 */
uint8_t maze_generate_route_for_speedrun(struct maze_route_list **route,
		maze_data *data, uint8_t target_x, uint8_t target_y);

/*
 * Returned values
 * 0 - route generated
 * 1 - no way to target
 * 2 - route is through not visited squares
 */
uint8_t maze_generate_route_for_speedrun_with_diagonals(struct maze_route_list **route,
		maze_data *data, uint8_t target_x, uint8_t target_y, uint8_t turns_in_row_to_make_diagonal);

static inline void maze_map_set_bit(uint8_t (*map)[16], maze_map_bits bit, uint8_t x,
		uint8_t y) {
	*(*(map + y) + x) |= (1 << bit);
}

static inline void maze_map_reset_bit(uint8_t (*map)[16], maze_map_bits bit, uint8_t x,
		uint8_t y) {
	*(*(map + y) + x) &= ~(1 << bit);
}

static inline uint8_t maze_map_check_bit(uint8_t (*map)[16], maze_map_bits bit,
		uint8_t x, uint8_t y) {
	return (*(*(map + y) + x)) & (1 << bit);
}

static inline uint8_t maze_get_floodfill_value(uint8_t (*tab_flood)[16], uint8_t x,
		uint8_t y) {
	return *(*(tab_flood + y) + x);
}

static inline void maze_set_floodfill_value(uint8_t (*tab_flood)[16], uint8_t x,
		uint8_t y, uint8_t value) {
	*(*(tab_flood + y) + x) = value;
}

static inline maze_local_dirs maze_opposite_local_dir(maze_local_dirs dir) {
	switch (dir) {
	case MAZE_LOCAL_DIR_RIGHT:
		return MAZE_LOCAL_DIR_LEFT;
	case MAZE_LOCAL_DIR_LEFT:
		return MAZE_LOCAL_DIR_RIGHT;
	case MAZE_LOCAL_DIR_AHEAD:
		return MAZE_LOCAL_DIR_BEHIND;
	case MAZE_LOCAL_DIR_BEHIND:
		return MAZE_LOCAL_DIR_AHEAD;
	case MAZE_LOCAL_DIR_AHEAD + MAZE_LOCAL_DIR_LEFT:
		return MAZE_LOCAL_DIR_BEHIND + MAZE_LOCAL_DIR_RIGHT;
	case MAZE_LOCAL_DIR_AHEAD + MAZE_LOCAL_DIR_RIGHT:
		return MAZE_LOCAL_DIR_BEHIND + MAZE_LOCAL_DIR_LEFT;
	case MAZE_LOCAL_DIR_BEHIND + MAZE_LOCAL_DIR_LEFT:
		return MAZE_LOCAL_DIR_AHEAD + MAZE_LOCAL_DIR_RIGHT;
	case MAZE_LOCAL_DIR_BEHIND + MAZE_LOCAL_DIR_RIGHT:
		return MAZE_LOCAL_DIR_AHEAD + MAZE_LOCAL_DIR_LEFT;
	}
	return 0;
}

static inline void maze_set_position(maze_data* data, uint8_t x, uint8_t y, maze_global_dirs course){
	data->cur_pos.course=course;
	data->cur_pos.x=x;
	data->cur_pos.y=y;
}

static inline uint8_t maze_move_is_diagonal(struct maze_route_list * move) {
	if (move->global_direction == MAZE_GLOBAL_DIR_NW
			|| move->global_direction == MAZE_GLOBAL_DIR_NE
			|| move->global_direction == MAZE_GLOBAL_DIR_SW
			|| move->global_direction == MAZE_GLOBAL_DIR_SE) {
		return 1;
	}
	return 0;
}


void maze_add_wall(uint8_t (*map)[16], uint8_t x, uint8_t y,
		maze_global_dirs dirs);

void maze_add_wall_from_position(uint8_t (*map)[16], maze_position pos,
		maze_local_dirs side);

uint8_t maze_check_if_wall_discovered(uint8_t (*map)[16], uint8_t x, uint8_t y,
		maze_global_dirs dir);

uint8_t maze_check_wall(uint8_t (*map)[16], uint8_t x, uint8_t y,
		maze_global_dirs dir);

void maze_delete_wall(uint8_t (*map)[16], uint8_t x, uint8_t y,
		maze_global_dirs dirs);

void maze_delete_wall_from_position(uint8_t (*map)[16], maze_position pos,
		maze_local_dirs side);

void maze_delete_bit_from_whole_map(uint8_t (*map)[16], maze_map_bits bit);

maze_global_dirs maze_turn_course_CW(maze_position* pos);

maze_global_dirs maze_turn_course_CCW(maze_position* pos);

maze_global_dirs maze_turn_course_CW_45(maze_position *pos);

maze_global_dirs maze_turn_course_CWW_45(maze_position *pos);

void maze_move_position_forward(maze_position* pos);

void maze_map_add_8x8_walls(uint8_t (*map)[16]);
void maze_map_add_16x16_walls(uint8_t (*map)[16]);

void maze_set_zeros_tab_16x16(uint8_t (*tab)[16]);

struct maze_route_list* maze_route_list_add_on_end(
		struct maze_route_list ** route, maze_local_dirs direction,
		maze_global_dirs global_direction, float distance);

struct maze_route_list* maze_route_list_get_from_top(
		struct maze_route_list ** route);

void maze_route_list_delete(struct maze_route_list**route);

//zamienia floodfill w trase po malejacych polach
//dalej nagradza jazde prosto
//jesli blad to zwracam 1
//jesli ok to 0
//jesli trasa nie zostala odwiedzona, to zwracam 2
//wyznaczam trase od starty (0,0), do mety tak jak floodfill wyznaczy�, koncze jak dojade do tej mety
uint8_t maze_floodfill_to_route_for_speedrun(uint8_t (*floodfill)[16],
		uint8_t (*map)[16], struct maze_route_list** route, uint8_t target_x,
		uint8_t target_y, maze_position start_pos);

void maze_optimize_route_list(struct maze_route_list** route);

void maze_optimize_route_list_add_diagonal(struct maze_route_list** route,
		uint8_t turns_in_row_to_make_diag);
void maze_do_floodfill(uint8_t (*tab_flood)[16], uint8_t (*map)[16], uint8_t start_x,
		uint8_t start_y);
void maze_do_floodfill_turn_is_worse(uint8_t (*tab_flood)[16], uint8_t (*map)[16],
		maze_position target_pos, uint8_t turn_weight,
		uint8_t turn_around_weight);

#endif /* APPLICATION_USER_LABIRYNT_H_ */
