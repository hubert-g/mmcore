/*
MIT License

Copyright (c) 2015-2018 Hubert Grzegorczyk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include "math.h"

#include "position_real.h"
#include "speed_pos_profiler.h"

/*
 * Euler turn data for a=1.
 * x,y,L
 */
static const float euler_turn_data[EULER_TURN_INDEX_MAX][4]={
		{0.417681,0.012166,0.417771,M_PI/36.0},
		{0.589200,0.034329,0.590817,M_PI/18.0},
		{0.719043,0.062941,0.723601,M_PI/12.0},
		{0.825849,0.096533,0.835542,M_PI/9.0},
		{0.995345,0.174974,1.023326,M_PI/6.0},
};



void speed_pos_profiler_init_data(speed_pos_data *data,speeds *curr_speeds){
	data->curr_speed=0;
	data->dest_speed=0;
	data->state_trans=SPEED_POS_PROFILER_STATE_FINISHED;

	data->curr_speed_rot=0;
	data->dest_speed_rot=0;
	data->state_rot=SPEED_POS_PROFILER_STATE_FINISHED;

	data->state_euler_turn=SPEED_POS_PROFILER_STATE_FINISHED;

	curr_speeds->v_rot=0;
	curr_speeds->v_trans=0;
}


void speed_pos_set_ideal_position(speed_pos_data *data,position_real current_position){
	data->ideal_current_pos=current_position;
	data->ideal_position_at_move_end=current_position;

}

void speed_pos_profiler_step(speed_pos_data *data, float deltaT,
		speeds *current_speed_given, position_real *current_position_given) {


	//ta pozycja przesuwa sie wzdluz ruchu aktualnego
	data->ideal_current_pos.x += data->curr_speed * data->move_direction
			* deltaT * cosf(data->ideal_current_pos.theta);

	data->ideal_current_pos.y += data->curr_speed * data->move_direction
			* deltaT * sinf(data->ideal_current_pos.theta);

	data->ideal_current_pos.theta += data->curr_speed_rot
			* data->move_direction_rot * deltaT;

	data->ideal_current_pos.theta=parse_angle_to_minus_pi_to_pi(data->ideal_current_pos.theta);

	*current_position_given = data->ideal_current_pos;


	////////////////////////////profiler przesuniecia
	if (data->state_trans == SPEED_POS_PROFILER_STATE_RUNNING) {
		float needed_braking_distance = (((data->curr_speed
				* (data->curr_speed - data->final_speed)) / data->braking_acc)
				- (((data->curr_speed - data->final_speed)
						* (data->curr_speed - data->final_speed))
						/ (2 * data->braking_acc)));

		if (needed_braking_distance >= data->distance_to_move) {
			data->state_trans = SPEED_POS_PROFILER_STATE_BRAKING;
			data->dest_speed = data->final_speed;
			float deltaV=data->curr_speed-data->final_speed;
			data->braking_acc=(deltaV*data->final_speed+(deltaV*deltaV)/2.0)/data->distance_to_move;
		}
	}

	if (data->curr_speed < data->dest_speed) {
		data->curr_speed += (data->acc * deltaT);

		if (data->curr_speed > data->dest_speed) {
			data->curr_speed = data->dest_speed;
		}

	} else if (data->curr_speed > data->dest_speed) {
		data->curr_speed -= (data->braking_acc * deltaT);

		if (data->curr_speed < data->dest_speed) {
			data->curr_speed = data->dest_speed;
		}
	}

	data->distance_to_move -= data->curr_speed * deltaT;

	if (data->state_trans != SPEED_POS_PROFILER_STATE_FINISHED) {

		if (data->distance_to_move <= 0
				|| (data->state_trans == SPEED_POS_PROFILER_STATE_BRAKING
						&& SMALL_ERROR >= fabsf(data->curr_speed - data->dest_speed))) {
			data->state_trans = SPEED_POS_PROFILER_STATE_FINISHED;
			data->ideal_current_pos=data->ideal_position_at_move_end;
		}
	}

	//powinien byc za przesunieciem poniewaz wtedy wyznaczam kat na podstawie nowej pozycji od razu.
	///////////////////////PROFILER ZAKRETU (2 polaczone klotoidy)
	if (data->state_euler_turn == SPEED_POS_PROFILER_STATE_RUNNING) {

		//kat w radianach jest rowny L^2/(2*a^2) - L przejechana do tej pory droga
		float delta_trans = (data->euler_turn_half_L*2)-data->distance_to_move;
		if (delta_trans < 0) {
			delta_trans = 0;
		}
		if (delta_trans <= data->euler_turn_half_L) {
			data->curr_speed_rot = 2 * delta_trans
					/ (2 * data->euler_turn_a * data->euler_turn_a);
			data->curr_speed_rot *= data->curr_speed;
		} else {
			delta_trans = 2 * data->euler_turn_half_L - delta_trans;
			data->curr_speed_rot = 2 * delta_trans
					/ (2 * data->euler_turn_a * data->euler_turn_a);
			data->curr_speed_rot *= data->curr_speed;

		}

		//warunek konca
		if (data->state_trans == SPEED_POS_PROFILER_STATE_FINISHED) {
			data->state_euler_turn = SPEED_POS_PROFILER_STATE_FINISHED;
			data->ideal_current_pos=data->ideal_position_at_move_end;
			data->curr_speed_rot=0;

		}
	}

	///////////////////////PROFILER ZAKRETU (krzywa przejsciowa + luk +
	//krzywa przejsciowa, symetryczne) krzywa przejsciowa to klotoida
	if (data->state_turn == SPEED_POS_PROFILER_STATE_RUNNING) {

		//kat w radianach jest rowny L^2/(2*a^2) - L przejechana do tej pory droga
		//ile juz przejechano
		float delta_trans = (data->turn_transition_L*2+data->turn_arc_L)-data->distance_to_move;
		if (delta_trans < 0) {
			delta_trans = 0;
		}
		if (delta_trans <= data->turn_transition_L) {
			data->curr_speed_rot = 2 * delta_trans
					/ (2 * data->turn_transition_a * data->turn_transition_a);
			data->curr_speed_rot *= data->curr_speed;
		}else if (delta_trans <= data->turn_transition_L+data->turn_arc_L){
			//jazda lukiem
			data->curr_speed_rot = data->curr_speed / data->turn_radius;
		}else {
			//krzywa wyjsciowa
			delta_trans = 2 * data->turn_transition_L + data->turn_arc_L - delta_trans;
			data->curr_speed_rot = 2 * delta_trans
					/ (2 * data->turn_transition_a * data->turn_transition_a);
			data->curr_speed_rot *= data->curr_speed;
		}

		//warunek konca
		if (data->state_trans == SPEED_POS_PROFILER_STATE_FINISHED) {
			data->state_turn = SPEED_POS_PROFILER_STATE_FINISHED;
			data->ideal_current_pos=data->ideal_position_at_move_end;
			data->curr_speed_rot=0;
		}
	}

	//////////////////profiler obrotu
	if (data->state_rot == SPEED_POS_PROFILER_STATE_RUNNING) {
		float needed_braking_distance = (((data->curr_speed_rot
				* (data->curr_speed_rot - data->final_speed_rot)) / data->braking_acc_rot)
				- (((data->curr_speed_rot - data->final_speed_rot)
						* (data->curr_speed_rot - data->final_speed_rot))
						/ (2 * data->braking_acc_rot)));

		if (needed_braking_distance >= data->distance_to_move_rot) {
			data->state_rot = SPEED_POS_PROFILER_STATE_BRAKING;
			data->dest_speed_rot = data->final_speed;
			float deltaV=data->curr_speed_rot-data->final_speed_rot;
			data->braking_acc_rot=(deltaV*data->final_speed_rot+(deltaV*deltaV)/2.0)/data->distance_to_move_rot;
		}
	}

	if (data->curr_speed_rot < data->dest_speed_rot) {
		data->curr_speed_rot += (data->acc_rot * deltaT);

		if (data->curr_speed_rot > data->dest_speed_rot) {
			data->curr_speed_rot = data->dest_speed_rot;
		}

	} else if (data->curr_speed_rot > data->dest_speed_rot) {
		data->curr_speed_rot -= (data->braking_acc_rot * deltaT);

		if (data->curr_speed_rot < data->dest_speed_rot) {
			data->curr_speed_rot = data->dest_speed_rot;
		}
	}

	data->distance_to_move_rot -= data->curr_speed_rot * deltaT;

	if (data->state_rot != SPEED_POS_PROFILER_STATE_FINISHED) {

		if (data->distance_to_move_rot <= 0
				|| (data->state_rot == SPEED_POS_PROFILER_STATE_BRAKING
						&& SMALL_ERROR >= fabsf(data->curr_speed_rot - data->dest_speed_rot))) {
			data->state_rot = SPEED_POS_PROFILER_STATE_FINISHED;
			data->ideal_current_pos=data->ideal_position_at_move_end;
		}
	}

	current_speed_given->v_rot=data->curr_speed_rot*data->move_direction_rot;
	current_speed_given->v_trans=data->curr_speed*data->move_direction;

}

void speed_pos_euler_spiral_turn_90(speed_pos_data *data,float dx_dy, float speed, float end_speed,
		float lin_acc, float lin_deacc, int8_t direction) {
	data->euler_turn_a = dx_dy
			/ (EULER_SPIRAL_1_45DEG_X + EULER_SPIRAL_1_45DEG_Y);
	data->euler_turn_half_L = data->euler_turn_a * EULER_SPIRAL_1_45DEG_L;

	data->ideal_position_at_move_start=data->ideal_position_at_move_end;
	data->ideal_current_pos=data->ideal_position_at_move_start;

	data->ideal_position_at_move_end.x+=dx_dy*cosf(data->ideal_position_at_move_end.theta);
	data->ideal_position_at_move_end.y+=dx_dy*sinf(data->ideal_position_at_move_end.theta);

	data->ideal_position_at_move_end.theta += direction * M_PI_2;

	data->ideal_position_at_move_end.x+=dx_dy*cosf(data->ideal_position_at_move_end.theta);
	data->ideal_position_at_move_end.y+=dx_dy*sinf(data->ideal_position_at_move_end.theta);

	data->ideal_position_at_move_end.theta = parse_angle_to_minus_pi_to_pi(
			data->ideal_position_at_move_end.theta);

	if(direction>0){
		data->move_direction_rot = 1;
	}else{
		data->move_direction_rot = -1;
	}

	//odpalic ruch do przodu (2*half L)
	data->dest_speed=speed;
	data->acc=lin_acc;
	data->braking_acc=lin_deacc;

	data->final_speed=end_speed;

	data->move_direction = 1;
	data->distance_to_move = data->euler_turn_half_L * 2;

	data->dest_speed_rot = data->curr_speed_rot = 0;
	data->state_trans=SPEED_POS_PROFILER_STATE_RUNNING;

	//pilnowac kata
	data->state_euler_turn = SPEED_POS_PROFILER_STATE_RUNNING;
}

/*
 *
 *
 *                         /
 *                        /
 *                       /
 *                      / b
 *                     /
 *                    /
 * __________________/alpha)
 *          b
 *  Theta is the angle of transition curve on bothe sides of turn.
 *  Turn is symmetric. It is composed from two euler spirals and and arc part.
 *  Everything in radians, m/s, m/s^2. Alpha must be 30-150 degrees. If it is
 *  needed to travel in reverse, b should be negative.
 *
 */
uint8_t speed_pos_turn(speed_pos_data *data, float b,
		speed_pos_euler_turn_data_index theta, float alpha, float speed,
		float lin_acc, float lin_deacc, float end_speed) {

	if(b<0){
		data->move_direction = -1;
	}else{
		data->move_direction = 1;
	}

	if(alpha>0){
		data->move_direction_rot = 1;
	}else{
		data->move_direction_rot = -1;
	}

	speed=fabsf(speed);
	lin_acc=fabsf(lin_acc);
	lin_deacc=fabsf(lin_deacc);
	end_speed=fabsf(end_speed);
	b = fabsf(b);
	alpha = fabsf(alpha);

	//calculate everything on absolute values but remember directions

	float beta = alpha - 2 * euler_turn_data[theta][3];

	if(2*euler_turn_data[theta][3] > alpha){
		return 1; //error, nie da sie zlozyc zakretu
	}

	if (alpha < M_PI / 6 || alpha > M_PI / 6 * 5) {
		return 1;
	}

	data->turn_transition_a = b
			/ (euler_turn_data[theta][0]
					+ (cosf(euler_turn_data[theta][3]) * sinf(beta / 2)
							+ sinf(euler_turn_data[theta][3]) * cosf(beta / 2)
							- sinf(euler_turn_data[theta][3]))
							/ euler_turn_data[theta][2]
					+ tanf( alpha / 2)
							* (euler_turn_data[theta][1]
									+ (sinf(euler_turn_data[theta][3])
											* sinf(beta / 2)
											- cosf(euler_turn_data[theta][3])
													* cosf(beta / 2)
											+ cosf(euler_turn_data[theta][3]))
											/ euler_turn_data[theta][2]));

	data->turn_transition_L = data->turn_transition_a
			* euler_turn_data[theta][2];
	data->turn_radius = data->turn_transition_a / euler_turn_data[theta][2];
	data->turn_arc_L = beta * data->turn_radius;

	data->ideal_position_at_move_start=data->ideal_position_at_move_end;
	data->ideal_current_pos = data->ideal_position_at_move_start;

	data->ideal_position_at_move_end.x += b * data->move_direction
			* cosf(data->ideal_position_at_move_end.theta);
	data->ideal_position_at_move_end.y += b * data->move_direction
			* sinf(data->ideal_position_at_move_end.theta);

	data->ideal_position_at_move_end.theta += data->move_direction_rot * alpha;

	data->ideal_position_at_move_end.theta = parse_angle_to_minus_pi_to_pi(
			data->ideal_position_at_move_end.theta);

	data->ideal_position_at_move_end.x += b * data->move_direction
			* cosf(data->ideal_position_at_move_end.theta);
	data->ideal_position_at_move_end.y += b * data->move_direction
			* sinf(data->ideal_position_at_move_end.theta);


	//odpalic ruch do przodu (2 * krzywa przejsciowa + luk)
	data->dest_speed=speed;
	data->acc=lin_acc;
	data->braking_acc=lin_deacc;

	data->final_speed=end_speed;


	data->distance_to_move = data->turn_transition_L * 2
			+ data->turn_arc_L;

	data->dest_speed_rot = data->curr_speed_rot = 0;
	data->state_trans=SPEED_POS_PROFILER_STATE_RUNNING;

	//pilnowac kata
	data->state_turn = SPEED_POS_PROFILER_STATE_RUNNING;

	return 0; //ok
}


/*
 * m , m/s , m/s^2
 */
void speed_pos_forward(speed_pos_data *data,float speed,float acc, float deacc,float distance,float final_speed){

	data->ideal_position_at_move_start=data->ideal_position_at_move_end;
	data->ideal_current_pos=data->ideal_position_at_move_start;

	data->ideal_position_at_move_end.x+=distance*cosf(data->ideal_position_at_move_end.theta);
	data->ideal_position_at_move_end.y+=distance*sinf(data->ideal_position_at_move_end.theta);

	data->dest_speed=speed;
	data->acc=acc;
	data->braking_acc=deacc;

	data->final_speed=final_speed;

	if(distance>0){
		data->move_direction=1;
		data->distance_to_move=distance;
	}else{
		data->distance_to_move=-distance;
		data->move_direction=-1;
	}

	data->dest_speed_rot = data->curr_speed_rot = 0;
	data->state_trans=SPEED_POS_PROFILER_STATE_RUNNING;
}

/*
 * rad and rad/s , rad/s^2
 */
void speed_pos_rotate(speed_pos_data *data,float speed,float acc, float deacc,float distance,float final_speed){
	data->ideal_position_at_move_start=data->ideal_position_at_move_end;
	data->ideal_current_pos=data->ideal_position_at_move_start;

	data->ideal_position_at_move_end.theta+=distance;

	data->ideal_position_at_move_end.theta = parse_angle_to_minus_pi_to_pi(
			data->ideal_position_at_move_end.theta);

	data->dest_speed_rot=speed;
	data->acc_rot=acc;
	data->braking_acc_rot=deacc;
	data->final_speed_rot=final_speed;

	if(distance>0){
		data->move_direction_rot=1;
		data->distance_to_move_rot=distance;
	}else{
		data->move_direction_rot=-1;
		data->distance_to_move_rot=-distance;
	}

	data->dest_speed = data->curr_speed = 0;
	data->state_rot=SPEED_POS_PROFILER_STATE_RUNNING;

}



