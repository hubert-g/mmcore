/*
MIT License

Copyright (c) 2015-2018 Hubert Grzegorczyk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include <math.h>
#include "position_real.h"

/*
 * Gives angular distance (shortest) for two values beetwen -pi to pi.
 */
float angular_distance(float from,float to){


	if(to-from>=M_PI){
		return -M_PI*2.0+(to-from);
	}else if(to-from<=-M_PI){
		return +M_PI*2.0+(to-from);
	}

	return to-from;

}


/*
 * Parses angle to -pi to pi
 */
float parse_angle_to_minus_pi_to_pi(float angle) {
	//parsuje kat
	while (angle > 2 * M_PI) {
		angle -= 2 * M_PI;
	}

	while (angle < -2 * M_PI) {
		angle += 2 * M_PI;
	}

	if (angle < -M_PI) {
		angle = 2 * M_PI + angle;
	}

	if (angle > M_PI) {
		angle = -2 * M_PI + angle;
	}
	return angle;
	// mam kat -180 - 180 , 180 sie pokrywa
}
