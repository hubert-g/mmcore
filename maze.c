/*
MIT License

Copyright (c) 2015-2018 Hubert Grzegorczyk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#include <maze.h>
#include <stdlib.h>
#include <math.h>
#include "mem_management.h"

/*
 * N
 * ^ y
 * |
 * |
 * |
 * |
 * |
 * ----------------> x E
 * (0,0)
 *
 */


void maze_init_data(maze_data *data,uint8_t start_x,uint8_t start_y,maze_global_dirs start_dir){
	maze_set_zeros_tab_16x16(data->map);
	maze_set_zeros_tab_16x16(data->floodfill);
	data->cur_pos.course=start_dir;
	data->cur_pos.x=start_x;
	data->cur_pos.y=start_y;

	maze_map_add_16x16_walls(data->map);

}

/*
 * Returned values
 * 0 - generated next move
 * 1 - target achieved
 * 2 - no way to target
 */
uint8_t maze_searching_get_next_move_to_target(maze_data *data,
		uint8_t left_wall_on_cur_pos, uint8_t right_wall_on_cur_pos,
		uint8_t front_wall_on_cur_pos,uint8_t target_x, uint8_t target_y ,maze_local_dirs *next_move) {

	//important! bit visited must be checked
	//based on this bit i can check if wall was discovered or not
	maze_map_set_bit(data->map,MAZE_MAP_BIT_VISITED,data->cur_pos.x,data->cur_pos.y);

	if (front_wall_on_cur_pos) {
		maze_add_wall_from_position(data->map,data->cur_pos,MAZE_LOCAL_DIR_AHEAD);
	}

	if (right_wall_on_cur_pos) {
		maze_add_wall_from_position(data->map,data->cur_pos,MAZE_LOCAL_DIR_RIGHT);
	}

	if (left_wall_on_cur_pos) {
		maze_add_wall_from_position(data->map,data->cur_pos,MAZE_LOCAL_DIR_LEFT);
	}

	maze_do_floodfill(data->floodfill, data->map, target_x, target_y); // pozycja mety

	maze_position temp_pos = data->cur_pos;
	maze_position temp_posL = data->cur_pos; //lewa
	maze_position temp_posR = data->cur_pos; //prawa
	maze_position temp_posB = data->cur_pos; //tyl

	maze_move_position_forward(&temp_pos);

	maze_turn_course_CW(&temp_posR);
	maze_move_position_forward(&temp_posR);

	maze_turn_course_CCW(&temp_posL);
	maze_move_position_forward(&temp_posL);

	maze_turn_course_CW(&temp_posB);
	maze_turn_course_CW(&temp_posB);
	maze_move_position_forward(&temp_posB);

	//The highest priority have move forward
	if ((maze_get_floodfill_value(data->floodfill, temp_pos.x, temp_pos.y)
			== maze_get_floodfill_value(data->floodfill, data->cur_pos.x, data->cur_pos.y) - 1)
			&& maze_check_wall(data->map, data->cur_pos.x, data->cur_pos.y, temp_pos.course) == 0) {
		*next_move=MAZE_LOCAL_DIR_AHEAD;
	} else if ((maze_get_floodfill_value(data->floodfill, temp_posR.x, temp_posR.y)
			== maze_get_floodfill_value(data->floodfill, data->cur_pos.x, data->cur_pos.y) - 1)
			&& maze_check_wall(data->map, data->cur_pos.x, data->cur_pos.y, temp_posR.course) == 0) { //right
		*next_move=MAZE_LOCAL_DIR_RIGHT;
	} else if ((maze_get_floodfill_value(data->floodfill, temp_posL.x, temp_posL.y)
			== maze_get_floodfill_value(data->floodfill, data->cur_pos.x, data->cur_pos.y) - 1)
			&& maze_check_wall(data->map, data->cur_pos.x, data->cur_pos.y, temp_posL.course) == 0) { //left
		*next_move=MAZE_LOCAL_DIR_LEFT;
	} else if ((maze_get_floodfill_value(data->floodfill, temp_posB.x, temp_posB.y)
			== maze_get_floodfill_value(data->floodfill, data->cur_pos.x, data->cur_pos.y) - 1)
			&& maze_check_wall(data->map, data->cur_pos.x, data->cur_pos.y, temp_posB.course) == 0) { //turn around
		*next_move=MAZE_LOCAL_DIR_BEHIND;
	} else {
		/*
		 * Either robot is on target pos or there is no way to target
		 */
		if(data->cur_pos.x == target_x && data->cur_pos.y == target_y){
			return 1; //target achieved
		}
		return 2; //no way to target
	}

	return 0; //Normal work, generated next move
}

/*
 * 0 - route generated
 * 1 - no way to target
 * 2 - route is through not visited squares
 */
uint8_t maze_generate_route_for_speedrun(struct maze_route_list **route,
		maze_data *data, uint8_t target_x, uint8_t target_y) {

	uint8_t ret;

	maze_route_list_delete(route); //delete if there was previously generated route

	maze_position target_pos;
	target_pos.x = target_x;
	target_pos.y = target_y;
	maze_do_floodfill(data->floodfill, data->map, target_x,target_y);

	ret = maze_floodfill_to_route_for_speedrun(data->floodfill, data->map,
			route, target_x, target_y, data->cur_pos);

	if(ret==0){
		maze_optimize_route_list(route);
		return 0;
	}else{
		maze_route_list_delete(route);
		return ret;
	}

}

uint8_t maze_generate_route_for_speedrun_with_diagonals(struct maze_route_list **route,
		maze_data *data, uint8_t target_x, uint8_t target_y, uint8_t turns_in_row_to_make_diagonal){
	uint8_t ret;

	maze_route_list_delete(route); //delete if there was previously generated route

	maze_do_floodfill(data->floodfill, data->map, target_x,target_y);

	ret = maze_floodfill_to_route_for_speedrun(data->floodfill, data->map,
			route, target_x, target_y, data->cur_pos);

	if(ret==0){
		maze_optimize_route_list_add_diagonal(route,turns_in_row_to_make_diagonal);
		return 0;
	}else{
		maze_route_list_delete(route);
		return ret;
	}

}


void maze_add_wall(uint8_t (*map)[16], uint8_t x, uint8_t y,
		maze_global_dirs dirs) {
	if (dirs & MAZE_GLOBAL_DIR_S) {
		if (y - 1 >= 0) {
			maze_map_set_bit(map, MAZE_MAP_BIT_WALL_N, x, y - 1);
		}
		maze_map_set_bit(map, MAZE_MAP_BIT_WALL_S, x, y);
	}

	if (dirs & MAZE_GLOBAL_DIR_N) {
		if (y + 1 <= 15) {
			maze_map_set_bit(map, MAZE_MAP_BIT_WALL_S, x, y + 1);
		}
		maze_map_set_bit(map, MAZE_MAP_BIT_WALL_N, x, y);
	}

	if (dirs & MAZE_GLOBAL_DIR_W) {
		if (x - 1 >= 0) {
			maze_map_set_bit(map, MAZE_MAP_BIT_WALL_E, x - 1, y);
		}
		maze_map_set_bit(map, MAZE_MAP_BIT_WALL_W, x, y);
	}

	if (dirs & MAZE_GLOBAL_DIR_E) {
		if (x + 1 <= 15) {
			maze_map_set_bit(map, MAZE_MAP_BIT_WALL_W, x + 1, y);

		}
		maze_map_set_bit(map, MAZE_MAP_BIT_WALL_E, x, y);
	}
}


void maze_add_wall_from_position(uint8_t (*map)[16], maze_position pos,
		maze_local_dirs side) {
	switch (side) {
	case MAZE_LOCAL_DIR_AHEAD:
		break;
	case MAZE_LOCAL_DIR_RIGHT:
		maze_turn_course_CW(&pos);
		break;
	case MAZE_LOCAL_DIR_LEFT:
		maze_turn_course_CCW(&pos);
		break;
	case MAZE_LOCAL_DIR_BEHIND:
		maze_turn_course_CCW(&pos);
		maze_turn_course_CCW(&pos);
		break;
	}
	maze_add_wall(map,pos.x, pos.y, pos.course);
}

//returns false if we dont know if there is wall
uint8_t maze_check_if_wall_discovered(uint8_t (*map)[16], uint8_t x, uint8_t y,
		maze_global_dirs dir){

	if (maze_map_check_bit(map, MAZE_MAP_BIT_VISITED, x, y)) {
		return 1;
	}
	//check if at least one of the sides of the wall was visited
	switch(dir){
	case MAZE_GLOBAL_DIR_N:
		if ((y < 15) && maze_map_check_bit(map,MAZE_MAP_BIT_VISITED,x,y+1))
		{
			return 1;
		}
		return 0;
		break;
	case MAZE_GLOBAL_DIR_S:
		if ((y > 0) && maze_map_check_bit(map,MAZE_MAP_BIT_VISITED,x,y-1))
		{
			return 1;
		}
		return 0;
		break;
	case MAZE_GLOBAL_DIR_E:
		if ((x < 15) && maze_map_check_bit(map,MAZE_MAP_BIT_VISITED,x + 1,y))
		{
			return 1;
		}
		return 0;
		break;
	case MAZE_GLOBAL_DIR_W:
		if ((x > 0) && maze_map_check_bit(map,MAZE_MAP_BIT_VISITED,x - 1,y))
		{
			return 1;
		}
		return 0;
		break;
	}
	return 0;
}


uint8_t maze_check_wall(uint8_t (*map)[16], uint8_t x, uint8_t y,
		maze_global_dirs dir){

	switch(dir){
	case MAZE_GLOBAL_DIR_N:
		return maze_map_check_bit(map,MAZE_MAP_BIT_WALL_N,x,y);
		break;
	case MAZE_GLOBAL_DIR_S:
		return maze_map_check_bit(map,MAZE_MAP_BIT_WALL_S,x,y);
		break;
	case MAZE_GLOBAL_DIR_E:
		return maze_map_check_bit(map,MAZE_MAP_BIT_WALL_E,x,y);
		break;
	case MAZE_GLOBAL_DIR_W:
		return maze_map_check_bit(map,MAZE_MAP_BIT_WALL_W,x,y);
		break;
	}
	return 0;

}

void maze_delete_wall(uint8_t (*map)[16], uint8_t x, uint8_t y,
		maze_global_dirs dirs) {
	if (dirs & MAZE_GLOBAL_DIR_S) {
		if (y - 1 >= 0) {
			maze_map_reset_bit(map, MAZE_MAP_BIT_WALL_N, x, y - 1);
		}
		maze_map_reset_bit(map, MAZE_MAP_BIT_WALL_S, x, y);
	}

	if (dirs & MAZE_GLOBAL_DIR_N) {
		if (y + 1 <= 15) {
			maze_map_reset_bit(map, MAZE_MAP_BIT_WALL_S, x, y + 1);
		}
		maze_map_reset_bit(map, MAZE_MAP_BIT_WALL_N, x, y);
	}

	if (dirs & MAZE_GLOBAL_DIR_W) {
		if (x - 1 >= 0) {
			maze_map_reset_bit(map, MAZE_MAP_BIT_WALL_E, x - 1, y);
		}
		maze_map_reset_bit(map, MAZE_MAP_BIT_WALL_W, x, y);
	}

	if (dirs & MAZE_GLOBAL_DIR_E) {
		if (x + 1 <= 15) {
			maze_map_reset_bit(map, MAZE_MAP_BIT_WALL_W, x + 1, y);

		}
		maze_map_reset_bit(map, MAZE_MAP_BIT_WALL_E, x, y);
	}
}

void maze_delete_wall_from_position(uint8_t (*map)[16], maze_position pos,
		maze_local_dirs side) {
	switch (side) {
	case MAZE_LOCAL_DIR_AHEAD:
		break;
	case MAZE_LOCAL_DIR_RIGHT:
		maze_turn_course_CW(&pos);
		break;
	case MAZE_LOCAL_DIR_LEFT:
		maze_turn_course_CCW(&pos);
		break;
	case MAZE_LOCAL_DIR_BEHIND:
		maze_turn_course_CCW(&pos);
		maze_turn_course_CCW(&pos);
		break;
	}

	maze_delete_wall(map, pos.x, pos.y, pos.course);
}


void maze_delete_bit_from_whole_map(uint8_t (*map)[16], maze_map_bits bit) {

	uint8_t x, y;
	for (y = 0; y < 16; ++y) {
		for (x = 0; x < 16; ++x) {
			maze_map_reset_bit(map, MAZE_MAP_BIT_WALL_E, x, y);
			}
		}
}



maze_global_dirs maze_turn_course_CW(maze_position* pos) { //obraca i zwraca kurs jak chce


	if (pos->course == MAZE_GLOBAL_DIR_W) {
		pos->course = MAZE_GLOBAL_DIR_N;
	} else if (pos->course == MAZE_GLOBAL_DIR_NW) {
		pos->course = MAZE_GLOBAL_DIR_NE;
	} else if (pos->course == MAZE_GLOBAL_DIR_SW) {
		pos->course = MAZE_GLOBAL_DIR_NW;
	} else {
		pos->course = pos->course >> 1;
	}
	return pos->course;
}

maze_global_dirs maze_turn_course_CCW(maze_position* pos) { //obraca i zwraca kurs jak chce

	if (pos->course == MAZE_GLOBAL_DIR_N) {
		pos->course = MAZE_GLOBAL_DIR_W;
	} else if (pos->course == MAZE_GLOBAL_DIR_NE) {
		pos->course = MAZE_GLOBAL_DIR_NW;
	} else if (pos->course == MAZE_GLOBAL_DIR_NW) {
		pos->course = MAZE_GLOBAL_DIR_SW;
	} else {
		pos->course = pos->course << 1;
	}
	return pos->course;
}

maze_global_dirs maze_turn_course_CW_45(maze_position *pos) {
	switch (pos->course){
	case MAZE_GLOBAL_DIR_N:
		pos->course = MAZE_GLOBAL_DIR_NE;
		break;
	case MAZE_GLOBAL_DIR_NE:
		pos->course = MAZE_GLOBAL_DIR_E;
		break;
	case MAZE_GLOBAL_DIR_E:
		pos->course = MAZE_GLOBAL_DIR_SE;
	break;
	case MAZE_GLOBAL_DIR_SE:
		pos->course = MAZE_GLOBAL_DIR_S;
		break;
	case MAZE_GLOBAL_DIR_S:
		pos->course = MAZE_GLOBAL_DIR_SW;
		break;
	case MAZE_GLOBAL_DIR_SW:
		pos->course = MAZE_GLOBAL_DIR_W;
	break;
	case MAZE_GLOBAL_DIR_W:
		pos->course = MAZE_GLOBAL_DIR_NW;
	break;
	case MAZE_GLOBAL_DIR_NW:
		pos->course = MAZE_GLOBAL_DIR_N;
	break;
	}
	return pos->course;

}

maze_global_dirs maze_turn_course_CWW_45(maze_position *pos) {
	switch (pos->course){
	case MAZE_GLOBAL_DIR_N:
		pos->course = MAZE_GLOBAL_DIR_NW;
		break;
	case MAZE_GLOBAL_DIR_NW:
		pos->course = MAZE_GLOBAL_DIR_W;
		break;
	case MAZE_GLOBAL_DIR_W:
		pos->course = MAZE_GLOBAL_DIR_SW;
	break;
	case MAZE_GLOBAL_DIR_SW:
		pos->course = MAZE_GLOBAL_DIR_S;
		break;
	case MAZE_GLOBAL_DIR_S:
		pos->course = MAZE_GLOBAL_DIR_SE;
		break;
	case MAZE_GLOBAL_DIR_SE:
		pos->course = MAZE_GLOBAL_DIR_E;
	break;
	case MAZE_GLOBAL_DIR_E:
		pos->course = MAZE_GLOBAL_DIR_NE;
	break;
	case MAZE_GLOBAL_DIR_NE:
		pos->course = MAZE_GLOBAL_DIR_N;
	break;
	}
	return pos->course;

}


void maze_move_position_forward(maze_position* pos) { //przesuwa w dana strone pozycje
	switch (pos->course) {
	case MAZE_GLOBAL_DIR_N:
		if (pos->y < 15) {
			(pos->y)++;
		}
		break;
	case MAZE_GLOBAL_DIR_S:
		if (pos->y > 0) {
			(pos->y)--;
		}
		break;
	case MAZE_GLOBAL_DIR_W:
		if (pos->x > 0) {
			(pos->x)--;
		}
		break;
	case MAZE_GLOBAL_DIR_E:
		if (pos->x < 15) {
			(pos->x)++;
		}
		break;
	}
}

void maze_map_add_8x8_walls(uint8_t (*map)[16]) {

	uint8_t x, y;

	y = 0;
	for (x = 0; x < 8; ++x) {
		maze_add_wall(map,x,y,MAZE_GLOBAL_DIR_S);
	}

	y = 7;
	for (x = 0; x < 8; ++x) {
		maze_add_wall(map,x,y,MAZE_GLOBAL_DIR_N);
	}

	x = 0;
	for (y = 0; y < 8; ++y) {
		maze_add_wall(map,x,y,MAZE_GLOBAL_DIR_W);
	}

	x = 7;
	for (y = 0; y < 8; ++y) {
		maze_add_wall(map,x,y,MAZE_GLOBAL_DIR_E);
	}

}

void maze_map_add_16x16_walls(uint8_t (*map)[16]) {

	uint8_t x, y;
	y = 0;
	for (x = 0; x < 16; ++x) {
		maze_add_wall(map,x,y,MAZE_GLOBAL_DIR_S);
	}

	y = 15;
	for (x = 0; x < 16; ++x) {
		maze_add_wall(map,x,y,MAZE_GLOBAL_DIR_N);
	}

	x = 0;
	for (y = 0; y < 16; ++y) {
		maze_add_wall(map,x,y,MAZE_GLOBAL_DIR_W);
	}

	x = 15;
	for (y = 0; y < 16; ++y) {
		maze_add_wall(map,x,y,MAZE_GLOBAL_DIR_E);
	}

}

void maze_set_zeros_tab_16x16(uint8_t (*tab)[16]){
	uint8_t x, y;
	for (y = 0; y < 16; ++y) {
		for (x = 0; x < 16; ++x) {
			*(*(tab + y) + x) = 0;
		}

	}
}


struct maze_route_list* maze_route_list_add_on_end(struct maze_route_list ** route,maze_local_dirs direction,maze_global_dirs global_direction,float distance ){

		if(route==NULL){ //
			return NULL;
		}

		if (*route == NULL) {
			*route = (struct maze_route_list*) malloc_adapt(sizeof(struct maze_route_list));
			if (*route == NULL) {
				return NULL;
			}
			(*route)->direction = direction;
			(*route)->distance = distance;
			(*route)->global_direction = global_direction;
			(*route)->next=NULL;

		} else {
			struct maze_route_list* temp = *route;
			while (temp->next != NULL) {
				temp = temp->next;
			}
			temp->next = (struct maze_route_list*) malloc_adapt(sizeof(struct maze_route_list));
			if (temp->next == NULL) {
				return NULL;
			}

			temp->next->direction=direction;
			temp->next->distance=distance;
			temp->next->global_direction = global_direction;
			temp->next->next = NULL;
			return temp->next;
		}
		return NULL;
}

struct maze_route_list*  maze_route_list_get_from_top(struct maze_route_list ** route ){

	if(route == NULL){
		return NULL;
	}

	if(*route==NULL){
		return NULL;
	}

	struct maze_route_list* temp=*route;
	*route=(*route)->next;
	return temp;
}


void maze_route_list_delete(struct maze_route_list**route){

	struct maze_route_list* temp=maze_route_list_get_from_top(route);
	while(temp!=NULL){
		free_adapt(temp);
		temp=maze_route_list_get_from_top(route);
	}

}

//zamienia floodfill w trase po malejacych polach
//dalej nagradza jazde prosto
//jesli blad to zwracam 1
//jesli ok to 0
//jesli trasa nie zostala odwiedzona, to zwracam 2
//wyznaczam trase od starty (0,0), do mety tak jak floodfill wyznaczy�, koncze jak dojade do tej mety
//if pointer on route list is null then route will be not created (this able to test if path is ok)
uint8_t maze_floodfill_to_route_for_speedrun(uint8_t (*floodfill)[16],uint8_t (*map)[16],struct maze_route_list** route, uint8_t target_x,uint8_t target_y,maze_position start_pos){
	uint8_t error=0;
	uint8_t not_visited=0;
	while (!(start_pos.x == target_x && start_pos.y == target_y) && error==0 && not_visited ==0) {
		maze_position temp_pos = start_pos;
		maze_position temp_posL = start_pos; //lewa
		maze_position temp_posR = start_pos; //prawa
		maze_position temp_posB = start_pos; //tyl

		temp_pos = start_pos;
		maze_move_position_forward(&temp_pos);

		maze_turn_course_CW(&temp_posR);
		maze_move_position_forward(&temp_posR);

		maze_turn_course_CCW(&temp_posL);
		maze_move_position_forward(&temp_posL);

		maze_turn_course_CW(&temp_posB);
		maze_turn_course_CW(&temp_posB);
		maze_move_position_forward(&temp_posB);

		if ((maze_get_floodfill_value(floodfill, temp_pos.x, temp_pos.y)
				== maze_get_floodfill_value(floodfill, start_pos.x, start_pos.y)
						- 1)
				&& maze_check_wall(map, start_pos.x, start_pos.y,
						temp_pos.course) == 0) {
			maze_move_position_forward(&start_pos);
			maze_route_list_add_on_end(route, MAZE_LOCAL_DIR_AHEAD,
					temp_pos.course, 0.18);

			//JADE W PRAWO
		} else if ((maze_get_floodfill_value(floodfill, temp_posR.x,
				temp_posR.y)
				== maze_get_floodfill_value(floodfill, start_pos.x, start_pos.y)
						- 1)
				&& maze_check_wall(map, start_pos.x, start_pos.y,
						temp_posR.course) == 0) { //prawo

			maze_turn_course_CW(&start_pos);
			maze_move_position_forward(&start_pos);
			maze_route_list_add_on_end(route, MAZE_LOCAL_DIR_RIGHT,
					temp_posR.course, 0.09);

			//JADE W LEWO
		} else if ((maze_get_floodfill_value(floodfill,temp_posL.x,temp_posL.y)
				== maze_get_floodfill_value(floodfill,start_pos.x,start_pos.y) - 1)
				&& maze_check_wall(map,start_pos.x,start_pos.y,temp_posL.course) == 0){ //lewo

			maze_turn_course_CCW(&start_pos);
			maze_move_position_forward(&start_pos);
			maze_route_list_add_on_end(route,MAZE_LOCAL_DIR_LEFT,temp_posL.course,0.09);

			//ZAWRACAM
		} else if ((maze_get_floodfill_value(floodfill,temp_posB.x,temp_posB.y)
				== maze_get_floodfill_value(floodfill,start_pos.x,start_pos.y) - 1)
				&& maze_check_wall(map,start_pos.x,start_pos.y,temp_posB.course) == 0) { //tyl

			maze_turn_course_CCW(&start_pos);
			maze_turn_course_CCW(&start_pos);
			maze_move_position_forward(&start_pos);
			maze_route_list_add_on_end(route,MAZE_LOCAL_DIR_BEHIND,temp_posB.course,0);

		} else {
			// nie ma dojazdu do konca, jest problem
			error=1;
		}
		if(!maze_map_check_bit(map,MAZE_MAP_BIT_VISITED,start_pos.x,start_pos.y)){
			not_visited++;
		}
	}
	if(error){
		return 1; //cannot find path to target
	}

	if(not_visited){
		return 2; //path is going through not visited square
	}
	//dodaje dojazd do srodka mety! pomaga pozniej
	//dzieki temu robot nie wjedzie ukosem na mete nigdy
	maze_route_list_add_on_end(route, MAZE_LOCAL_DIR_AHEAD,
			start_pos.course, 0.0915); //daje wiecej, dluzsza droga hamowania + zawsze bedzie na koncu hamowanie AHEAD!
	//ostatnim ruchem zawsze bedzie ahead

return 0;
}

void maze_optimize_route_list(struct maze_route_list** route) {
	struct maze_route_list* temp = *route;
	if (temp != NULL) {
		while (temp->next != NULL) {
			if (temp->direction == temp->next->direction && temp->direction==MAZE_LOCAL_DIR_AHEAD) {
				struct maze_route_list* to_delete = temp->next;
				(temp->distance) += temp->next->distance;
				temp->next=to_delete->next;
				free_adapt(to_delete);
			}else{
				temp=temp->next;
			}

		}
	}
}

void maze_optimize_route_list_add_diagonal(struct maze_route_list** route,
		uint8_t turns_in_row_to_make_diag) {
	struct maze_route_list* temp = *route;
	struct maze_route_list* new = NULL;
	if (temp == NULL) {
		return;
	}

	if (turns_in_row_to_make_diag < 2) {
		turns_in_row_to_make_diag = 2; //nie moze byc mniej bo zalozenia mam takie
	}

	//optymalizacja jazdy na wprost
	while (temp->next != NULL) {
		if (temp->direction == temp->next->direction && temp->direction == MAZE_LOCAL_DIR_AHEAD) {
			struct maze_route_list* to_delete = temp->next;
			(temp->distance) += temp->next->distance;
			temp->next = to_delete->next;
			free_adapt(to_delete);
		} else {
			temp = temp->next;
		}

	}
	//zmiana sekwencji lewo,prawo itd na skosy -- skos lewo to LEFT+AHEAD i jedzie przez distance kratek
	temp=*route;
	while (temp->next != NULL) {
		if (temp->direction == MAZE_LOCAL_DIR_RIGHT || temp->direction == MAZE_LOCAL_DIR_LEFT) {
			uint8_t dir = temp->direction;
			uint8_t count = 1; //jedna kratka juz jest do usuniecia
			//teraz licze ile bedzie kratek jazdy po ukosie, jak mniej niz X to sie nie oplaca
			struct maze_route_list *iter = temp;
			while (iter->next != NULL
					&& iter->next->direction == maze_opposite_local_dir(dir)) {
				++count;
				dir = iter->next->direction;
				iter = iter->next;
			}
			//count zawiera ile kratek przejade po ukosie a temp zaiwera pierwsza kratke i jej kierunek zakretu
			if (count >= turns_in_row_to_make_diag) {
				//okresl kierunek w jakim bede jechal
				maze_position temp_pos;
				temp_pos.course=temp->global_direction;
				if(temp->direction == MAZE_LOCAL_DIR_LEFT){
					//odwrotnie obracam bo to jest global_dir po zakrecie
					maze_turn_course_CW_45(&temp_pos);
				}else{
					maze_turn_course_CWW_45(&temp_pos);
				}
				temp->global_direction=temp_pos.course;
				temp->direction += MAZE_LOCAL_DIR_AHEAD; //robie ukos
				temp->distance = (float)count * 0.09 * M_SQRT2;


				//skasowac pozostale niepotrzbne
				//od temp->next wlacznie wedlug count
				for (int i = 1; i < count; ++i) {
					struct maze_route_list* to_delete = temp->next;
					temp->next = to_delete->next;
					free_adapt(to_delete);
				}
			} else {
				if (temp->next != NULL) {
					temp = temp->next;
				}
			}
		} else {
			temp = temp->next;
		}
	}
	//teraz ahead+right/left oznacza ca�y ukos razem z obrotem na poczatku i koncu
	//startuje z przeciecia kratek i obracam si� w miejscu wedlug tego.
	//distance jest rzeczywisty, dla pojedynczych ruch�w jest 0, dla reszty w m

	//wsadzenie miedzy skosy krzywych przejsciowych
	//teraz kierunku ahead/behind+left/right beda oznaczac krzywe przejsciowe
	temp=*route;
	while (temp->next != NULL) {
		if(temp->direction == MAZE_LOCAL_DIR_AHEAD && !maze_move_is_diagonal(temp) && temp->next->direction == MAZE_LOCAL_DIR_AHEAD_LEFT){
			temp->distance -= 0.09;
			temp->next->direction = MAZE_LOCAL_DIR_AHEAD;
			temp->next->distance -= 0.09;
			new = (struct maze_route_list*) malloc_adapt(sizeof(struct maze_route_list));
			new->distance=0.09;
			new->direction=MAZE_LOCAL_DIR_AHEAD_LEFT;
			new->global_direction=temp->next->global_direction;
			new->next=temp->next;
			temp->next=new;
			temp=temp->next->next;
		}else if(temp->direction == MAZE_LOCAL_DIR_AHEAD && !maze_move_is_diagonal(temp) && temp->next->direction == MAZE_LOCAL_DIR_AHEAD_RIGHT){
			temp->distance -= 0.09;
			temp->next->direction = MAZE_LOCAL_DIR_AHEAD;
			temp->next->distance -= 0.09;
			new = (struct maze_route_list*) malloc_adapt(sizeof(struct maze_route_list));
			new->distance=0.09;
			new->direction=MAZE_LOCAL_DIR_AHEAD_RIGHT;
			new->global_direction=temp->next->global_direction;
			new->next=temp->next;
			temp->next=new;
			temp=temp->next->next;
			//pierwszy ruch to od razu ukos, musze to obejsc, bo nie da sie tak zrobic!
			//dlatego daje ruch w miejscu z promieniem zero (obrot w miejscu)
		}else if(temp->direction == MAZE_LOCAL_DIR_AHEAD_RIGHT && maze_move_is_diagonal(temp) && temp==*route){
			temp->direction = MAZE_LOCAL_DIR_AHEAD;
			new = (struct maze_route_list*) malloc_adapt(sizeof(struct maze_route_list));
			new->distance=0.0;
			new->direction=MAZE_LOCAL_DIR_AHEAD_RIGHT;
			new->global_direction=temp->global_direction;
			new->next=temp;
			*route=new;
		}else if(temp->direction == MAZE_LOCAL_DIR_AHEAD_LEFT && maze_move_is_diagonal(temp) && temp==*route){
			temp->direction = MAZE_LOCAL_DIR_AHEAD;
			new = (struct maze_route_list*) malloc_adapt(sizeof(struct maze_route_list));
			new->distance=0.0;
			new->direction=MAZE_LOCAL_DIR_AHEAD_LEFT;
			new->global_direction=temp->global_direction;
			new->next=temp;
			*route=new;
		}else if(temp->direction == MAZE_LOCAL_DIR_LEFT && !maze_move_is_diagonal(temp) && temp->next->direction == MAZE_LOCAL_DIR_AHEAD_LEFT){
			//zamiast zakr�tu robi�
			temp->next->direction = MAZE_LOCAL_DIR_AHEAD;
			temp->next->distance -= (0.18-0.09 * M_SQRT2);
			maze_position temp_pos;
			temp_pos.course=temp ->global_direction;
			temp->global_direction = maze_turn_course_CWW_45(&temp_pos);
			temp->distance=0.18;
			temp->direction=MAZE_LOCAL_DIR_BEHIND_LEFT;
			temp=temp->next;
		}else if(temp->direction == MAZE_LOCAL_DIR_RIGHT && !maze_move_is_diagonal(temp) && temp->next->direction == MAZE_LOCAL_DIR_AHEAD_RIGHT){
			//zamiast zakr�tu robi�
			temp->next->direction = MAZE_LOCAL_DIR_AHEAD;
			temp->next->distance -= (0.18-0.09 * M_SQRT2);
			maze_position temp_pos;
			temp_pos.course=temp ->global_direction;
			temp->global_direction = maze_turn_course_CW_45(&temp_pos);
			temp->distance=0.18;
			temp->direction=MAZE_LOCAL_DIR_BEHIND_RIGHT;
			temp=temp->next;
			//z ukosa w ukos
		}else if(temp->direction == MAZE_LOCAL_DIR_AHEAD && maze_move_is_diagonal(temp) && temp->next->direction == MAZE_LOCAL_DIR_AHEAD_LEFT){
			temp->distance -= 0.09 ;
			temp->next->direction = MAZE_LOCAL_DIR_AHEAD;
			temp->next->distance -= 0.09 ;
			new = (struct maze_route_list*) malloc_adapt(sizeof(struct maze_route_list));
			new->distance = 0.09 ;
			new->direction=MAZE_LOCAL_DIR_LEFT;
			new->global_direction=temp->next->global_direction;
			new->next=temp->next;
			temp->next=new;
			temp=temp->next->next;
		}else if(temp->direction == MAZE_LOCAL_DIR_AHEAD && maze_move_is_diagonal(temp) && temp->next->direction == MAZE_LOCAL_DIR_AHEAD_RIGHT){
			temp->distance -= 0.09 ;
			temp->next->direction = MAZE_LOCAL_DIR_AHEAD;
			temp->next->distance -= 0.09;
			new = (struct maze_route_list*) malloc_adapt(sizeof(struct maze_route_list));
			new->distance = 0.09 ;
			new->direction=MAZE_LOCAL_DIR_RIGHT;
			new->global_direction=temp->next->global_direction;
			new->next=temp->next;
			temp->next=new;
			temp=temp->next->next;
	//WYJAZDY Z UKOSOW
		}else if(temp->direction == MAZE_LOCAL_DIR_AHEAD && maze_move_is_diagonal(temp) && temp->next->direction == MAZE_LOCAL_DIR_RIGHT){
			//zamiast zakr�tu robi�
			temp->distance -= (0.18-0.09 * M_SQRT2);
			temp->next->distance=0.18;
			temp->next->direction=MAZE_LOCAL_DIR_BEHIND_RIGHT;
			temp=temp->next;
		}else if(temp->direction == MAZE_LOCAL_DIR_AHEAD && maze_move_is_diagonal(temp) && temp->next->direction == MAZE_LOCAL_DIR_LEFT){
			//zamiast zakr�tu robi�
			temp->distance -= (0.18-0.09 * M_SQRT2);
			temp->next->distance=0.18;
			temp->next->direction=MAZE_LOCAL_DIR_BEHIND_LEFT;
			temp=temp->next->next;
		}else if(temp->direction == MAZE_LOCAL_DIR_AHEAD && maze_move_is_diagonal(temp) && temp->next->direction == MAZE_LOCAL_DIR_AHEAD){
			temp->distance -= 0.09;
			temp->next->distance -= 0.09;
			new = (struct maze_route_list*) malloc_adapt(sizeof(struct maze_route_list));
			new->distance = 0.09;
			new->global_direction=temp->next->global_direction;
			new->next=temp->next;
			temp->next=new;
			//szukam w ktora strone zakret
			maze_position left_option;
			left_option.course = temp->global_direction;

			maze_turn_course_CWW_45(&left_option);
			if(left_option.course==temp->next->global_direction){
				new->direction=MAZE_LOCAL_DIR_AHEAD_LEFT;
			}else{
				new->direction=MAZE_LOCAL_DIR_AHEAD_RIGHT;
			}
			temp=temp->next->next;
		}else{
			temp=temp->next;
		}
	}

	//usuwam tam gdzie AHEAD distance jest poni�ej 1 mm (zakladam ze to rowne 0)
	temp = *route;
	while (temp->next != NULL) {
		if(temp->next->direction == MAZE_LOCAL_DIR_AHEAD && temp->next->distance<0.001){
			struct maze_route_list *to_delete=temp->next;
			temp->next=temp->next->next;
			free_adapt(to_delete);
		}else{
			temp=temp->next;
		}
	}

}


void maze_do_floodfill(uint8_t (*tab_flood)[16], uint8_t (*map)[16], uint8_t start_x,
		uint8_t start_y) {

	maze_set_zeros_tab_16x16(tab_flood);

	uint8_t end_cond = 1;
	uint8_t act_number = 1; //zaczynam od 1
	uint8_t x, y;

	maze_set_floodfill_value(tab_flood, start_x, start_y, 1);

	while (end_cond > 0) { //wype�niam az mam co wypelniac (licze ile razy jest kolejna liczba)
		end_cond = 0;
		for (y = 0; y < 16; ++y) { //przeszukuje w poszukiwaniu act_number
			for (x = 0; x < 16; ++x) {
				if (*(*(tab_flood + y) + x) == act_number) {
					end_cond++;

					if (maze_check_wall(map, x, y, MAZE_GLOBAL_DIR_N) == 0) {
						if (maze_get_floodfill_value(tab_flood, x, y + 1)
								== 0) {
							maze_set_floodfill_value(tab_flood, x, y + 1,
									act_number + 1);
						}
					}

					if (maze_check_wall(map, x, y, MAZE_GLOBAL_DIR_S) == 0) {
						if (maze_get_floodfill_value(tab_flood, x, y - 1)
								== 0) {
							maze_set_floodfill_value(tab_flood, x, y - 1,
									act_number + 1);
						}
					}

					if (maze_check_wall(map, x, y, MAZE_GLOBAL_DIR_W) == 0) {
						if (maze_get_floodfill_value(tab_flood, x - 1, y)
								== 0) {
							maze_set_floodfill_value(tab_flood, x - 1, y,
									act_number + 1);
						}
					}

					if (maze_check_wall(map, x, y, MAZE_GLOBAL_DIR_E) == 0) {
						if (maze_get_floodfill_value(tab_flood, x + 1, y)
								== 0) {
							maze_set_floodfill_value(tab_flood, x + 1, y,
									act_number + 1);
						}
					}

				}

			}

		}

		act_number++;
	}

}

void maze_do_floodfill_turn_is_worse(uint8_t (*tab_flood)[16], uint8_t (*map)[16],
		maze_position target_pos, uint8_t turn_weight,
		uint8_t turn_around_weight) {

	maze_set_zeros_tab_16x16(tab_flood);

	uint8_t dir_array[16][16];
	maze_set_zeros_tab_16x16(dir_array);

	uint8_t end_cond = 1;
	uint8_t act_number = 1; //zaczynam od 1
	uint8_t x, y;

	maze_set_floodfill_value(tab_flood, target_pos.x, target_pos.y, 1);
	*(*(dir_array + target_pos.y) + target_pos.x) = target_pos.course;

	while (end_cond > 0) { //wype�niam az mam co wypelniac (licze ile razy jest kolejna liczba)

		//wypisywanie krokow floodfill
		//wypisz_floodfill_w_mapie_z_pozycja(tab_flood, map,NULL);
		end_cond = 0;
		for (y = 0; y < 16; ++y) { //przeszukuje w poszukiwaniu act_number
			for (x = 0; x < 16; ++x) {
				if (*(*(tab_flood + y) + x) >= act_number) {
					end_cond++;

					maze_position forw;
					forw.x = x;
					forw.y = y;
					forw.course = *(*(dir_array + y) + x);
					maze_position left = forw;

					maze_turn_course_CCW(&left);
					maze_move_position_forward(&left);

					maze_position right = forw;
					maze_turn_course_CW(&right);
					maze_move_position_forward(&right);

					maze_position backward = forw;
					maze_turn_course_CCW(&backward);
					maze_turn_course_CCW(&backward);
					maze_move_position_forward(&backward);

					maze_move_position_forward(&forw);
					if (maze_check_wall(map, x, y, forw.course) == 0) {
						if (maze_get_floodfill_value(tab_flood, forw.x, forw.y)
								== 0) {
							maze_set_floodfill_value(tab_flood, forw.x, forw.y,
									maze_get_floodfill_value(tab_flood, x, y)
											+ 1);
							*(*(dir_array + forw.y) + forw.x) = forw.course;
						}
					}

					if (maze_check_wall(map, x, y, right.course) == 0) {
						if (maze_get_floodfill_value(tab_flood, right.x,
								right.y) == 0) {
							maze_set_floodfill_value(tab_flood, right.x,
									right.y,
									maze_get_floodfill_value(tab_flood, x, y)
											+ turn_weight);
							*(*(dir_array + right.y) + right.x) = right.course;
						}
					}


					if (maze_check_wall(map, x, y, left.course) == 0) {
						if (maze_get_floodfill_value(tab_flood, left.x, left.y)
								== 0) {
							maze_set_floodfill_value(tab_flood, left.x, left.y,
									maze_get_floodfill_value(tab_flood, x, y)
											+ turn_weight);
							*(*(dir_array + left.y) + left.x) = left.course;
						}
					}

					if (maze_check_wall(map, x, y, backward.course) == 0) {
						if (maze_get_floodfill_value(tab_flood, backward.x, backward.y)
								== 0) {
							maze_set_floodfill_value(tab_flood, backward.x, backward.y,
									maze_get_floodfill_value(tab_flood, x, y)
											+ turn_around_weight);
							*(*(dir_array + backward.y) + backward.x) = backward.course;
						}
					}

				}

			}

		}

		act_number++;

	}

}






