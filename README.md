# Micromouse core repository

Author: Hubert Grzegorczyk

License: MIT


> WARNING!! This code generates cancer.

If you have any questions, ask.

## Usage

arm_math library is needed, this library provides functions needed for matrix calculations and also better trigonometric functions implementations (optimized for STM32F4 DSP commands).

There should be provided functions:

```
int float_to_str(char * str, float value);

char check_wall_front();

char check_wall_left();

char check_wall_right();

char log_string(char *text, char free_text_after,unsigned int len);
```

Functions:

```
speed_pos_profiler_step

kalman_move_mean_predicted

kalman_step

```
should be called periodically.

Here is example of "main" program:

```
void robot_logic(void * p) {
	char* text;
	char buff[20];

	log_string("task started",0,0);
	//potrzebne zmienne, mapy itd



	position_real start_position;

	start_position.x =
	(START_DIR == MAZE_GLOBAL_DIR_N ? 0.09 : 0.048);
	start_position.y =
	(START_DIR == MAZE_GLOBAL_DIR_N ? 0.048 : 0.09);
	start_position.theta =
	(START_DIR == MAZE_GLOBAL_DIR_N ? M_PI_2 : 0.0);

	maze_init_data(&maze_dat, 0, 0, START_DIR);


	//kalman parameters
	kalman_data.params.var_x = 0.01;
	kalman_data.params.var_y = 0.01;
	kalman_data.params.var_theta = 0.02;
	kalman_data.params.var_vl = 3.0;
	kalman_data.params.var_theta_meas = 4.0;
	kalman_data.params.theta_meas_gap = 5;

	kalman_data.params.vl_predict_meas_allowed_delta = 0.05;

	kalman_data.params.distance_from_wall_edge = 0.01;
	kalman_data.params.distance_from_wall_edge_further_walls = 0.045;
	kalman_data.params.theta_meas_min_dist_between_2_meas = 0.02;
	kalman_data.params.search_box_size = 2;

//left vl
	kalman_data.params.sensors_position_in_robot_frame[0].x=0.0268;
	kalman_data.params.sensors_position_in_robot_frame[0].y=0.03086;
	kalman_data.params.sensors_position_in_robot_frame[0].theta=1.3089969389;
//front vl
	kalman_data.params.sensors_position_in_robot_frame[1].x=0.039;
	kalman_data.params.sensors_position_in_robot_frame[1].y=0;
	kalman_data.params.sensors_position_in_robot_frame[1].theta=0;
//right vl
	kalman_data.params.sensors_position_in_robot_frame[2].x=0.0268;
	kalman_data.params.sensors_position_in_robot_frame[2].y=-0.03086;
	kalman_data.params.sensors_position_in_robot_frame[2].theta=-1.3089969389;

	//init kalman data here once again to be sure! that variances are in matrices!
	kalman_init_data(&kalman_data, start_position);
	
	//mm parameters
	mm_data.distance_from_back_to_robot_rotate_center=0.042;
	mm_data.distance_from_square_cross_for_scanning=0.005;
	mm_data.transition_curve_theta=EULER_TURN_20DEG;
	mm_data.start_global_dir=START_DIR;

	mm_data.finish_x=7;
	mm_data.finish_y=7;

	mm_data.kalman_data=&kalman_data;
	mm_data.maze_dat=&maze_dat;
	mm_data.speed_pos_profiler_data=&speed_pos_profiler_data;
	mm_data.start_position=start_position;

	mm_data.stopped=0;
	mm_data.follow_path_rotate=0;
	mm_data.follow_path_straight=0;
	mm_data.follow_path_diagonal=0;
	mm_data.follow_path_turn=0;

	mm_data.scan_acc = 1.7;
	mm_data.scan_deacc = 1.3;

	mm_data.speedrun_acc = 3.7;
	mm_data.speedrun_deacc = 3.7;


	//to be sure that start position is initialized in both kalmman and pos profiler.
	kalman_set_position_to_known(mm_data.kalman_data, mm_data.start_position);
	speed_pos_set_ideal_position(mm_data.speed_pos_profiler_data, mm_data.start_position);

	float scan_speed = choose_with_wheel(15, 3, 0) * 0.050 + 0.150;
	uart_send("Wybrano predkosc: ");
	uart_float(scan_speed);
	uart_send("\r\n");

	ir_leds_sensors_turn_on_off(1);	// DOPIERO TUTAJ ODPALAM DIODY IR ponad 300mA

	neons_pwm_turn_on_off(1);

	motors_turn_on_off(1); //odpalam tak�e silniki

	vl_start_stop_from_another_task(0b111);

	delay_2_seconds_neons();

	//////////////

	text=pvPortMalloc(100);
	strcpy(text,"Scanning with speed: ");
	float_to_str(buff,scan_speed);
	strcat(text,buff);
	strcat(text," m/s");
	log_string(text,1,0);

	micromouse_scan_from_start_to_finish_and_back(&mm_data,scan_speed);

	ir_leds_sensors_turn_on_off(0);
	while(1){
		vl_start_stop_from_another_task(0b000);
		uint8_t map_is_not_complete=micromouse_check_if_sufficiently_mapped(&mm_data);
		float chosen_speed;
		float chosen_speed_turn;
		uint8_t chosen_option=0;

		if(map_is_not_complete){
			chosen_option = choose_with_wheel(3,3,map_is_not_complete);
		}else{
			chosen_option = choose_with_wheel(3,1,map_is_not_complete);
		}



		switch(chosen_option){
		case 1:
			neons_set(200, 0);
			chosen_speed =
					choose_with_wheel(15, 1, 0)
							* 0.200 + 0.600;
			neons_set(0, 200);
			chosen_speed_turn =
					choose_with_wheel(15, 1, 0)
							* 0.050 + 0.250;
			neons_set(0, 0);

			text=pvPortMalloc(100);
			strcpy(text,"Speeds chosen for speedrun - straight: ");
			float_to_str(buff,chosen_speed);
			strcat(text,buff);
			strcat(text," m/s, turn: ");
			float_to_str(buff,chosen_speed_turn);
			strcat(text,buff);
			strcat(text," m/s");
			log_string(text,1,0);

			vl_start_stop_from_another_task(0b111);
			delay_2_seconds_neons();
			neons_set(1000,1000);
			micromouse_speedrun(&mm_data,MICROMOUSE_NORMAL_SPEEDRUN,chosen_speed,chosen_speed_turn);
			neons_set(0,0);
			delay_ms(500); //this is time to stop the robot
			break;
		case 2:
			neons_set(200,0);
			chosen_speed =
					choose_with_wheel(15, 1, 0)
							* 0.200 + 0.600;

			neons_set(0, 200);
			chosen_speed_turn =
					choose_with_wheel(15, 1, 0)
							* 0.050 + 0.250;
			neons_set(0, 0);

			text=pvPortMalloc(100);
			strcpy(text,"Speeds chosen for speedrun - straight: ");
			float_to_str(buff,chosen_speed);
			strcat(text,buff);
			strcat(text," m/s, turn: ");
			float_to_str(buff,chosen_speed_turn);
			strcat(text,buff);
			strcat(text," m/s");
			log_string(text,1,0);

			vl_start_stop_from_another_task(0b111);
			delay_2_seconds_neons();
			neons_set(1000,1000);
			micromouse_speedrun(&mm_data,MICROMOUSE_DIAGONAL_SPEEDRUN,chosen_speed,chosen_speed_turn);
			neons_set(0,0);
			delay_ms(500); //this is time to stop the robot
			break;
		case 3:


			scan_speed = choose_with_wheel(15, 3, 0) * 0.050 + 0.150;

			text=pvPortMalloc(100);
			strcpy(text,"Scanning with speed: ");
			float_to_str(buff,scan_speed);
			strcat(text,buff);
			strcat(text," m/s");
			log_string(text,1,0);

			ir_leds_sensors_turn_on_off(1);
			vl_start_stop_from_another_task(0b111);
			delay_2_seconds_neons();
			micromouse_scan_from_start_to_finish_and_back(&mm_data,scan_speed);
			ir_leds_sensors_turn_on_off(0);
			break;

		}



	}


```